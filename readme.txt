Hexpound Help

Just some notes until I write help files.

The automatic check for update will try to download a file every 30 minutes to
see if you are using the newest version. If it does download the file, and you
are using the newest version, it will not try again until the next day. If you
are not using the newest version, it will notify you and not try to check again
until you start using a newer version.

For hex searching, you can enter some number in parentheses to search for that
many bytes that can match anything. For example,
47CB (2) FFF0 will match 47CB 87E0 FFF0 and 47CB FFFF FFF0 etc.
Also when hex searching, whitespace does not matter:
searching for A8 F 0 ( 1 )  87
is the same as A8F0(1)87

A table file has conversions for bytes in the hex pane to characters in the
character pane. A line in a table file looks like: 80=A
Tables can have empty lines and comments with the '#' character. All whitespace
left of the equal sign is ignored. Three escape sequences are supported on the
right side of the equal sign: '\t', '\r', '\n', and '\\'. You can set FF=\n and
any text separated by that will have a new line between it when you copy it.

If the 'Show only chars from table' option is selected, the char pane will only
show the table conversion for the bytes, not ASCII. If the loaded table cannot
convert a byte to a letter, the 'Unknown char' will be displayed.

Typing 0 in for the 'Bytes per line' option will have Hexpound try to
show as many bytes on a line as possible without having a scroll bar.

Whenever you press Ctrl+F or 'Find...', the range will be set to the selected
portion of the file. Be careful to not have any selected if you want to search
the whole file. If nothing is selected, the range will be turned off.

'Copy From Pane' does 'Copy Hex String' if the hex pane is selected or 'Copy
String' if the char pane is selected.

Scripts are written in BeanShell, a Java source interpreter.

You can click the caret offset in the status bar to copy it to the clipboard.
