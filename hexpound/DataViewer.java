/*
 * DataViewer.java - Shows the data at the caret
 * Copyright (C) 2004, 2005 Jordan Miner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package hexpound;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.GridLayout;
import java.awt.Point;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class DataViewer extends JDialog {
  private static DataViewer instance;

  private static JLabel byteLabel1 = new JLabel("Byte:  ");
  private static JLabel byteLabel2 = new JLabel("");
  private static JLabel unsignedByteLabel1 = new JLabel("Unsigned Byte:  ");
  private static JLabel unsignedByteLabel2 = new JLabel("");

  DataViewer() {
    super(Hexpound.getWindow(), "Data Viewer", false);
    JPanel pane = new JPanel(new GridLayout(2,2,5,5));
    pane.add(byteLabel1);
    pane.add(byteLabel2);
    pane.add(unsignedByteLabel1);
    pane.add(unsignedByteLabel2);
    getContentPane().add(pane);
    update();
    pack();
    Point pt = Hexpound.getWindow().getJMenuBar().getLocationOnScreen();
    setLocation(pt.x+Hexpound.getWindow().getContentPane().getWidth()-30-getWidth(), pt.y);
  }

  public static void update() {
    Buffer buffer = Hexpound.getCurrentBuffer();
    if(buffer==null) {
      byteLabel2.setText("");
      unsignedByteLabel2.setText("");
      return;
    }
    int caret = HexpoundWindow.getCurrentHexArea().getCaretPosition();
    if(caret != Hexpound.getCurrentBuffer().getLength()) {
      byteLabel2.setText(""+buffer.getByteAt(caret));
      unsignedByteLabel2.setText(""+(buffer.getByteAt(caret) < 0 ? buffer.getByteAt(caret)+256 : buffer.getByteAt(caret)));
    } else {
      byteLabel2.setText("");
      unsignedByteLabel2.setText("");
    }
  }
  public static DataViewer getDialog() {
    if(instance == null)
      instance = new DataViewer();
    return instance;
  }
  public void show() {
    super.show();
    Hexpound.getAction("data-viewer").setSelected(true);
  }
  public void hide() {
    super.hide();
    Hexpound.getAction("data-viewer").setSelected(false);
  }
}
