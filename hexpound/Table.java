/*
 * Table.java - A class that holds conversions between bytes and strings
 * Copyright (C) 2005 Jordan Miner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package hexpound;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

/*
 * This class holds and uses the data from a table file.
 */
public class Table {
  private File file;
  private long lastModified;

  private ArrayList byteList = new ArrayList();
  private ArrayList byteStrList = new ArrayList();

  public class ParseException extends Exception {
    public ParseException(String message, int line) {
      super("Error in table file on line "+line+":\n"+message);
    }
  }

  public Table() {
  }

  /**
   * @param text the text of the table file to parse
   */
  public Table(File file) throws IOException, ParseException {
    this.file = file;
    lastModified = file.lastModified();
    String[] lines = Hexpound.readFile(file);

    String hexStr = null;
    String str = null;
    int index = 0;
    for(int i=0; i<lines.length; i++) {
      //allow comments and empty lines
      if(lines[i].trim().length()==0 || lines[i].trim().indexOf('#')==0)
        continue;
      try {
        index = lines[i].indexOf('=');
        if(index == -1) //for now, must have an equal sign
          throw new Exception("No equal sign.");
        hexStr = lines[i].substring(0, index);
        hexStr = Hexpound.removeWhitespace(hexStr);
        str = lines[i].substring(index+1);
        str = str.replace("\\\\", "\\");
        str = str.replace("\\t", "\t");
        str = str.replace("\\r", "\r");
        str = str.replace("\\n", "\n");
        if(hexStr.length() == 2) {
          byteList.add(new Byte( (byte)Integer.parseInt(hexStr, 16) ));
          byteStrList.add(str);
        } else if(hexStr.length() == 4) {
          throw new Exception("Two-byte table values are not yet supported.");
        } else if(hexStr.length() > 4) {
          throw new Exception("Table values more than two bytes are not supported.");
        } else {
          throw new Exception("Invalid hex value.");
        }
      } catch(NumberFormatException ex) {
        System.out.println(ex.toString());
        throw new ParseException("Invalid hex value.", i+1);
      } catch(Exception ex) {
        System.out.println(ex.toString());
        throw new ParseException(ex.getMessage(), i+1);
      }
    }
  }

  public File getFile() {
    return file;
  }

  /**
   * Returns if the file has been changed on disk.
   * @return <code>true</code> if the file has been changed or
   *         deleted, <code>false</code> otherwise.
   */
  public boolean hasFileChanged() {
    if(file == null)
      return false;
    return lastModified < file.lastModified();
  }

  public boolean hasEntry(byte b) {
    return byteList.indexOf(new Byte(b)) != -1;
  }

  public String convert(byte b) {
    int index = byteList.indexOf(new Byte(b));
    if(index != -1)
      return (String)byteStrList.get(index);
    else
      return new String(new byte[] {b});
  }

  public String convert(byte[] b) {
    String str = "";
    String tmp = null;
    for(int i=0; i<b.length; i++) {
      str += convert(b[i]);
    }
    return str;
  }

  /*
   * Convert char to byte for typing.
   */
  public byte convert(char c) {
    for(int i=0; i<byteStrList.size(); i++) {
      if( byteStrList.get(i).equals( String.valueOf(c) ) )
        return ((Byte)byteList.get(i)).byteValue();
    }
    return new String(new char[] {c}).getBytes()[0];
  }
}
