/*
 * ByteArray.java - A collection class for bytes.
 * Copyright (C) 2005 Jordan Miner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package hexpound;

public class ByteArray {
  private byte[] data;
  private int size;

  public ByteArray() {
    this(16);
  }
  public ByteArray(int initialSize) {
    data = new byte[initialSize];
  }
  public final void add(byte b) {
    ensureCapacity(size+1);
    data[size] = b;
    size++;
  }
  public final void add(byte[] b) {
    ensureCapacity(size+b.length);
    System.arraycopy(b, 0, data, size, b.length);
    size = size+b.length;
  }
  public final byte get(int pos) {
    return data[pos];
  }
  public final byte[] toArray() {
    byte[] ret = new byte[size];
    System.arraycopy(data, 0, ret, 0, size);
    return ret;
  }
  public final void clear() {
    size = 0;
  }
  public final int getSize() {
    return size;
  }
  public final void setSize(int newSize) {
    size = newSize;
  }
  public final void ensureCapacity(int capacity) {
    if(data.length >= capacity)
      return;
    byte[] newData = new byte[Math.max((data.length+1)*2, capacity)];
    System.arraycopy(data, 0, newData, 0, size);
    data = newData;
  }
}
