/*
 * JHexArea.java - The hex area component
 * Copyright (C) 2004, 2005 Jordan Miner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package hexpound;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.*;
import java.awt.font.TextLayout;
import java.awt.Toolkit;
import java.io.IOException;
import java.util.Collections;
import javax.swing.*;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.EventListenerList;
import javax.swing.event.MouseInputAdapter;
import javax.swing.plaf.ComponentUI;

/*
 * Really speed up painting.
 */
/**
 * NOTE: if this is added to a JViewport, a ChangeListener will be added to the
 * JViewport, but will not be removed.
 */
public class JHexArea extends JComponent implements Scrollable {
  private Buffer buffer;
  private ChangeListener viewportListener;

  private int focusedPane = HEX_PANE;
  public static final int HEX_PANE = 1;
  public static final int CHARACTER_PANE = 2;
  private int mark = 0;
  private int caret = 0;
  private boolean atHalfByte = false;
  private int selectionStart = 0;
  private int selectionEnd = 0;

  private Rectangle hexPaneRect = new Rectangle(0,0,0,0);
  private Rectangle charPaneRect = new Rectangle(0,0,0,0);
  private boolean caretVisible = true;
  private Timer caretTimer = new Timer(500, null);

  private int charWidth;
  private int charHeight;
  private int bytesPerLine = 16;  //this is used in program

  //if 0, bytesPerLine will = as many as can fit
  private static int _bytesPerLine = 16;
  private static int grouping = 2;
  private static Font font = new Font("Monospaced", Font.PLAIN, 13);
  private static String unknownChar = ":";
  private static boolean showOnlyTableChars = false;
  private static boolean aaEnabled = false;
  private static boolean insertEnabled = false;
  private static Color selectedTextColor;
  private static Color selectionColor;
  private static Color offsetColumnColor = Color.BLACK;
  private static Color oddColumnColor = Color.BLACK;
  private static Color evenColumnColor = Color.BLACK;
  private static Color textColumnColor = Color.BLACK;
  private static Color caretColor = Color.BLACK;

  public JHexArea(Buffer b) {
    setBuffer(b);
    setFocusTraversalKeys(KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS, Collections.EMPTY_SET); //TODO: make CTRL-TAB
    addKeyListener(new KeyHandler());
    MouseHandler mouseHandler = new MouseHandler();
    addMouseListener(mouseHandler);
    addMouseMotionListener(mouseHandler);
    addFocusListener(new FocusHandler());

    updateUI();
    setTransferHandler(new FileTransferHandler());

    //TODO make scrollpane always show scroll bars and uncomment this??
    /*viewportListener = new ChangeListener() {
      public void stateChanged(ChangeEvent e) {
        if(getParent() != null)
          getParent().validate();
      }
    };
    addHierarchyListener(new HierarchyListener() {
      public void hierarchyChanged(HierarchyEvent e) {
        if((e.getChangeFlags() & HierarchyEvent.PARENT_CHANGED) != 0 && e.getChanged() == JHexArea.this) {
          if(e.getChangedParent() instanceof JViewport) {
            JViewport viewport = (JViewport)e.getChangedParent();
            viewport.addChangeListener(viewportListener);
          }
        }
      }
    });*/
  }
  /*protected void finalize() throws Throwable {
    super.finalize();
    System.out.println("JHexArea.finalize(), but this isn't being called :(");
  }*/
  public void showGoToOffsetDialog() {
    String input = JOptionPane.showInputDialog(Hexpound.getWindow(), "Enter offset:    (precede with 0x for hex) (+ or - for relative)", "Go to Offset", JOptionPane.QUESTION_MESSAGE);
    if(input == null)
      return;
    input = Hexpound.removeWhitespace(input);
    try {
      if(input.startsWith("+"))
        setCaretPosition( getClosestValidOffset(caret+Hexpound.decodeInt(input.substring(1))), false);
      else if(input.startsWith("-"))
        setCaretPosition( getClosestValidOffset(caret-Hexpound.decodeInt(input.substring(1))), false);
      else
        setCaretPosition(getClosestValidOffset(Hexpound.decodeInt(input)));
      scrollToCaret();
    } catch(NumberFormatException ex) {
      JOptionPane.showMessageDialog(Hexpound.getWindow(), "Invalid offset", "Error", JOptionPane.ERROR_MESSAGE);
    }
  }

  //{{{ display option methods
  public static void propertiesChanged() {
    _bytesPerLine = Hexpound.getIntegerProperty("bytes-per-line");
    grouping = Hexpound.getIntegerProperty("grouping");
    font = new Font("Monospaced", Hexpound.getIntegerProperty("font-style"), Hexpound.getIntegerProperty("font-size"));
    unknownChar = Hexpound.getProperty("unknown-char");
    showOnlyTableChars = Hexpound.getBooleanProperty("show-only-table-chars");
    aaEnabled = Hexpound.getBooleanProperty("smooth-text");
    offsetColumnColor = Hexpound.getColorProperty("offset-column-color");
    oddColumnColor = Hexpound.getColorProperty("odd-column-color");
    evenColumnColor = Hexpound.getColorProperty("even-column-color");
    textColumnColor = Hexpound.getColorProperty("text-column-color");
    caretColor = Hexpound.getColorProperty("caret-color");

    for(int i=0; i<Hexpound.getWindow().getHexAreaCount(); i++) {
      Hexpound.getWindow().getHexArea(i).invalidate();
      Hexpound.getWindow().getHexArea(i).repaint();
    }
  }
  public void updateUI() {
    super.updateUI();
    selectedTextColor = UIManager.getColor("TextArea.selectionForeground");
    selectionColor = UIManager.getColor("TextArea.selectionBackground");
    setForeground(UIManager.getColor("TextArea.foreground"));
    setBackground(UIManager.getColor("TextArea.background"));
  }
  //}}}

  public Buffer getBuffer() {
    return buffer;
  }
  public void setBuffer(Buffer b) {
    if(b==null)
      throw new NullPointerException("buffer must not be null");
    //TODO: should remove listener from old buffer
    buffer = b;
    buffer.addBufferChangeListener(new BufferChangeListener() {
      public void bufferChanged(Buffer buffer) {
        invalidate();
        repaint();
        if(caret > buffer.getLength() || mark > buffer.getLength())
          setCaretPosition(buffer.getLength(), false);
      }
    });
    invalidate();
    repaint();
  }
  //TODO: start using this in key handling code
  /** If offset is invalid, the closest valid offset is returned. */
  public int getClosestValidOffset(int offset) {
    if(offset < 0)
      offset = 0;
    if(offset > buffer.getLength())
      offset = buffer.getLength();
    return offset;
  }
  /*
   * Offset must be >=0 and <= the buffer length. Also, if the offset equals
   * the buffer length, atHalfByte must be false.
   */
  public void setCaretPosition(int offset, boolean atHalfByte) {
    if(offset < 0 || offset > buffer.getLength() || (offset == buffer.getLength() && atHalfByte))
      throw new IllegalArgumentException("offset out of bounds: "+offset);
    boolean shouldRepaint = caret!=offset || this.atHalfByte!=atHalfByte || mark != offset;

    caret = offset;
    this.atHalfByte = atHalfByte;
    mark = offset;
    caretTimer.restart();
    caretVisible = true;
    if(shouldRepaint) {
      repaint(); //TODO: possibly speed up??
    }
    scrollToCaret();
    fireCaretUpdate();
  }
  public void setCaretPosition(int offset) {
    setCaretPosition(offset, offset==buffer.getLength() ? false : atHalfByte);
  }
  public void moveCaretPosition(int offset) {
    if(offset < 0 || offset > buffer.getLength())
      throw new IllegalArgumentException("offset out of bounds: "+offset);
    boolean shouldRepaint = caret!=offset || atHalfByte!=false;
    int oldCaret = caret;

    caret = offset;
    atHalfByte = false;
    caretTimer.restart();
    caretVisible = true;
    if(shouldRepaint) {
      if(oldCaret < caret)
        repaintLines(getLineOfOffset(oldCaret), getLineOfOffset(caret)+1);
      else
        repaintLines(getLineOfOffset(caret), getLineOfOffset(oldCaret)+1);
    }
    scrollToCaret();
    fireCaretUpdate();
  }
  public int getMarkPosition() {
    return mark;
  }
  public int getCaretPosition() {
    return caret;
  }
  public int getSelectionStart() {
    if(mark < caret) {
      return mark;
    } else {
      return caret;
    }
  }
  public int getSelectionEnd() {
    if(mark > caret) {
      return mark;
    } else {
      return caret;
    }
  }
  /**
   * @return true if there is a selection, false otherwise
   */
  public boolean hasSelection() {
    return mark != caret;
  }
  public boolean isInsertEnabled() {
    return insertEnabled;
  }
  public void setInsertEnabled(boolean insert) {
    insertEnabled = insert;
    StatusBar.update();
  }
  /*
   * Returns HEX_PANE or CHARACTER_PANE
   */
  public int getFocusedPane() {
    return focusedPane;
  }
  public void setFocusedPane(int pane) {
    if(pane == HEX_PANE || pane == CHARACTER_PANE)
      focusedPane = pane;
    else
      throw new IllegalArgumentException("pane must be either HEX_PANE or CHARACTER_PANE");
    repaint();
  }

  public void setSelection(byte[] b) {
    buffer.insert(caret, b);
    setCaretPosition(caret+b.length);
    invalidate();
    repaint();
  }
  public void setSelection(String str) {
    if(mark != caret) //if selection
      delete();
    buffer.insert(caret, str);
    setCaretPosition(caret+str.length());
    invalidate();
    repaint();
  }
  public void backspace() {
    if(mark == caret) { //if no selection
      if(caret > 0) {
        setCaretPosition(caret-1, false); //move caret back
        buffer.delete(getSelectionStart(), getSelectionEnd()+1);//from new caret
      }
    } else {
      int selStart = getSelectionStart();
      buffer.delete(getSelectionStart(), getSelectionEnd());
      setCaretPosition(selStart, false);
    }
    repaint();
  }
  public void delete() {
    if(mark == caret) { //if no selection
      buffer.delete(getSelectionStart(), getSelectionEnd()+1);
      setCaretPosition(caret, false);
    } else {
      int selStart = getSelectionStart();
      buffer.delete(getSelectionStart(), getSelectionEnd());
      setCaretPosition(selStart, false);
    }
    repaint();
  }
  public void userInput(char c) {
    if(getFocusedPane() == HEX_PANE) {
      //{{{
      try {
        if(insertEnabled && !atHalfByte || caret==buffer.getLength()) {
          byte b = (byte)Integer.parseInt(c+"0", 16);
          if(mark != caret)
            delete();
          buffer.insert(caret, b);
          setCaretPosition(caret, true);
        } else {
          if(atHalfByte) {
            String str = getHexString(caret).substring(0, 1)+c;
            buffer.setByteAt(caret, (byte)Integer.parseInt(str, 16));
            setCaretPosition(caret+1, false);
          } else {
            assert caret < buffer.getLength();
            //if(caret < buffer.getLength()) {
              String str = c+getHexString(caret).substring(1, 2);
              buffer.setByteAt(caret, (byte)Integer.parseInt(str, 16));
              setCaretPosition(caret, true);
            //}
          }
        }
      } catch(NumberFormatException e) {
        Toolkit.getDefaultToolkit().beep();
      }
      //}}}
    } else {
      if(mark != caret)
        delete();
      if(insertEnabled || caret==buffer.getLength()) {
        buffer.insert(caret, c);
        setCaretPosition(caret+1, false);
      } else {
        buffer.setCharAt(caret, c);
        if(caret+1 <= buffer.getLength())
          setCaretPosition(caret+1, false);
      }
    }
  }
  public void selectAll() {
    mark = 0;
    moveCaretPosition(buffer.getLength());
    scrollToCaret();
    repaint();
  }

  //{{{ ByteAndStringSelection
  class ByteAndStringSelection implements Transferable {
    private byte[] b;
    private String str;
    public ByteAndStringSelection(byte[] b, String str) {
      this.b = b;
      this.str = str;
    }
    public DataFlavor[] getTransferDataFlavors() {
      return new DataFlavor[] {
        new DataFlavor(byte[].class, null),
        new DataFlavor(String.class, null)
      };
    }
    public boolean isDataFlavorSupported(DataFlavor flavor) {
      return flavor.getRepresentationClass() == String.class ||
             flavor.getRepresentationClass() == byte[].class;
    }
    public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException, IOException {
      if(flavor.getRepresentationClass() == String.class)
        return str;
      else if(flavor.getRepresentationClass() == byte[].class)
        return b;
      else
        throw new UnsupportedFlavorException(flavor);
    }
  }
  //}}}

  public void cut() {
    copy();
    if(hasSelection())
      delete();
  }

  public void copy() {
    byte[] bytes = getBuffer().getBytes(getSelectionStart(), getSelectionEnd());
    Clipboard cb = Toolkit.getDefaultToolkit().getSystemClipboard();
    String str = null;
    if(getFocusedPane() == JHexArea.HEX_PANE) {
      str = getSpacedHexString(getSelectionStart(), getSelectionEnd());
    } else if(getFocusedPane() == JHexArea.CHARACTER_PANE) {
      str = getDisplayableText(getSelectionStart(), getSelectionEnd());
    }
    cb.setContents(new ByteAndStringSelection(bytes, str), null);
  }

  public void copyString() {
    String str = getDisplayableText(getSelectionStart(), getSelectionEnd());
    Clipboard cb = Toolkit.getDefaultToolkit().getSystemClipboard();
    cb.setContents(new StringSelection(str), null);
  }

  public void copyHexString() {
    String str = getSpacedHexString(getSelectionStart(), getSelectionEnd());
    Clipboard cb = Toolkit.getDefaultToolkit().getSystemClipboard();
    cb.setContents(new StringSelection(str), null);
  }

  /* returns if anything was pasted */
  public boolean paste() {
      //if string on clipboard, don't paste unless focused pane is char pane
      //if bytes, just paste
      Transferable contents = Toolkit.getDefaultToolkit().getSystemClipboard().getContents(this);
      if(contents == null) {
        return false;
      }
      try {
        if(contents.isDataFlavorSupported(new DataFlavor(byte[].class, null))) {
          byte[] ba = (byte[])contents.getTransferData(new DataFlavor(byte[].class, null));
          if(hasSelection())
            delete();   //delete selection before pasting
          getBuffer().insert(getCaretPosition(), ba);
          //move caret to correct place
          setCaretPosition(getCaretPosition()+ba.length);
          scrollToCaret();
        } else if(contents.isDataFlavorSupported(DataFlavor.stringFlavor)) {
          String str = (String)contents.getTransferData(DataFlavor.stringFlavor);
          if(getFocusedPane() == JHexArea.HEX_PANE) {
            byte[] b = Hexpound.parseHex(str);
            setSelection(b);
          } else if(getFocusedPane() == JHexArea.CHARACTER_PANE) {
            setSelection(str);
          }
          scrollToCaret();
        }
      } catch(Exception ex) {
        return false;
      }
    return true;
  }

  //{{{ scrolling methods
  public void scrollToCaret() {
    Point loc = getLocationOfOffset(new OffsetInfo(getFocusedPane(), caret, atHalfByte));
    scrollRectToVisible(new Rectangle(loc.x, loc.y, 0, charHeight));
  }
  //}}}

  /**
   * Gets a hex string with the current spacing preserved.
   *
   * @param start the beginning offset, inclusive.
   * @param end   the ending offset, exclusive.
   */
  public String getSpacedHexString(int start, int end) {
    if(start < 0 || start > buffer.getLength())
      throw new IndexOutOfBoundsException("start: "+start);
    if(end < 0)
      throw new IndexOutOfBoundsException("end: "+end);
    if(end > buffer.getLength())
      end = buffer.getLength();

    int lineStartOffset = start/bytesPerLine*bytesPerLine;
    StringBuffer strBuffer = new StringBuffer();
    for(int i=start; i<end; i++) {
      strBuffer.append(getHexString(i));
      if((i-lineStartOffset+1)%grouping == 0)
        strBuffer.append(" ");
    }
    return strBuffer.toString();
  }

  /**
   * @param start the beginning offset, inclusive.
   * @param end   the ending offset, exclusive.
   */
  public String getHexString(int start, int end) {
    if(start < 0 || start > buffer.getLength())
      throw new IndexOutOfBoundsException("start: "+start);
    if(end < 0)
      throw new IndexOutOfBoundsException("end: "+end);
    if(end > buffer.getLength())
      end = buffer.getLength();

    StringBuffer strBuffer = new StringBuffer();
    for(int i=start; i<end; i++) {
      strBuffer.append(getHexString(i));
    }
    return strBuffer.toString();
  }
  public String getHexString(int index) {
    if(index < 0 || index >= buffer.getLength())
      throw new IndexOutOfBoundsException("index: "+index);

    //need to convert to positive before using toHexString()
    String str = Integer.toHexString(buffer.getByteAt(index) < 0 ? buffer.getByteAt(index)+256 : buffer.getByteAt(index));
    if(str.length() < 2)
      return "0"+str.toUpperCase();
    return str.toUpperCase();
  }

  /**
   * @param start the beginning offset, inclusive.
   * @param end   the ending offset, exclusive.
   */
  public String getDisplayableText(int start, int end) {
    if(start < 0 || start > buffer.getLength())
      throw new IndexOutOfBoundsException("start: "+start);
    if(end < 0)
      throw new IndexOutOfBoundsException("end: "+end);
    if(end > buffer.getLength())
      end = buffer.getLength();

    StringBuffer strBuffer = new StringBuffer();
    for(int i=start; i<end; i++) {
      strBuffer.append(getDisplayableText(i));
    }
    return strBuffer.toString();
  }
  private static final Table emptyTable = new Table();
  public String getDisplayableText(int index) {
    if(index < 0 || index >= buffer.getLength())
      throw new IndexOutOfBoundsException("index: "+index);

    String str = null;
    Table tbl = buffer.getTable();
    if(tbl == null)
      tbl = emptyTable;

    if(showOnlyTableChars && buffer.getTable()!=null) {
      if(tbl.hasEntry(buffer.getByteAt(index)))
        str = tbl.convert(buffer.getByteAt(index));
      else
        str = unknownChar;
    } else {
      str = tbl.convert(buffer.getByteAt(index));
    }
    //if str can be displayed
    if(font.canDisplayUpTo(str) == -1) {
      return str;
    } else {
      return unknownChar;
    }
  }

  public final int getCaretLine() {
    return caret/bytesPerLine;
  }
  public final int getLineOfOffset(int offset) {
    return offset/bytesPerLine;
  }
  public final int getLineStartOffset(int line) {
    return line*bytesPerLine;
  }
  /** Will not return a value greater than buffer.getLength() */
  public final int getLineEndOffset(int line) {
    return Math.min((line+1)*bytesPerLine-1, buffer.getLength());
  }
  public int getColumnAtLocation(Point location) {
    return (hexPaneRect.x + location.x) / ((grouping*2+1) * charWidth);
  }
  public Point getLocationOfOffset(OffsetInfo info) {
    Point retValue = new Point(0,0);
    retValue.y = charHeight*(int)(info.offset/bytesPerLine);
    if(info.pane==HEX_PANE) {
      retValue.x = hexPaneRect.x + charWidth*((info.offset%bytesPerLine)*2 + (info.offset%bytesPerLine)/grouping);
      if(info.atHalfByte)
        retValue.x += charWidth;
    }
    if(info.pane==CHARACTER_PANE) {
      retValue.x = charPaneRect.x + charWidth*(getDisplayableText( getLineStartOffset((int)(info.offset/bytesPerLine)), info.offset).length());
    }
    return retValue;
  }

  public Dimension getPreferredSize() {
    int prefWidth = 0;
    if(_bytesPerLine == 0) {
      prefWidth = getVisibleRect().width;
    } else {
      prefWidth = charPaneRect.x+charWidth*bytesPerLine;

      if(charHeight > 0) {
        Rectangle visibleRect = getVisibleRect();
        int firstLine = visibleRect.y/charHeight;
        int lastLine = 1+(visibleRect.y+visibleRect.height)/charHeight;
        int lineWidth = 0;
        for(int i=firstLine; i<=lastLine; i++) {
          int start = Math.min(getLineStartOffset(i), buffer.getLength());;
          int end = Math.min(getLineStartOffset(i)+bytesPerLine, buffer.getLength());
          lineWidth = getDisplayableText(start, end).length()*charWidth;
          prefWidth = Math.max(prefWidth, charPaneRect.x+lineWidth);
        }
      }
    }

    return new Dimension(prefWidth, charHeight*(1+buffer.getLength()/bytesPerLine));
  }

  /**
   * Calls repaint for the specified line.
   *
   * @param line the line to repaint, zero or more
   */
  public final void repaintLine(int line) {
    repaint(0, charHeight*line, getWidth(), charHeight);
  }
  /**
   * Calls repaint for the specified lines.
   *
   * @param start the first line to repaint, inclusive
   * @param end   the last line to repaint, exclusive
   */
  public final void repaintLines(int start, int end) {
    repaint(0, charHeight*start, getWidth(), charHeight*(end-start));
  }

  //{{{ paintComponent()
  //speed up by painting rects only on screen using clip bounds
    //**VERY IMPORTANT: didn't know this for a while: when drawing string,
    //the x and y are the location of the BASELINE of the text
  protected void paintComponent(Graphics g) {
    Graphics2D g2d = (Graphics2D)g.create();
    Rectangle clipRect = g2d.getClipBounds();

    if(aaEnabled)
      g2d.setRenderingHints(new RenderingHints(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON));
    g2d.setFont(font);
    TextLayout tl = new TextLayout("A", font, g2d.getFontRenderContext());
    charWidth = Math.round(tl.getAdvance());
    charHeight = (int)(tl.getAscent()+tl.getDescent());
    int ascent = (int)tl.getAscent();

    //{{{ find bytesPerLine
    //calculate dynamically
    bytesPerLine = _bytesPerLine;
    if(bytesPerLine<1) {
      //must have at least one displayed
      bytesPerLine = 1;
      for(int i=2; true; i++) {
        //offset column width + hex columns width + chars column width
        float neededWidth = charWidth*9 + charWidth*(i*2+i/grouping) + charWidth*i;
        //Container c = getParent();
        //c = (c!=null ? c : this);
        if(neededWidth>getWidth()) //c.getWidth()
          break;
        bytesPerLine = i;
      }
    }
    //}}}
    //{{{ find Rectangles for panes
    //offset width
    hexPaneRect.x = charWidth*9;
    hexPaneRect.y = 0;
    hexPaneRect.width = charWidth * (bytesPerLine*2 +
                        bytesPerLine/grouping +
                        (bytesPerLine % grouping == 0 ? 0 : 1));
    hexPaneRect.height = getHeight();
    charPaneRect.x = hexPaneRect.x + hexPaneRect.width;
    charPaneRect.y = 0;
    charPaneRect.width = getWidth() - charPaneRect.x;
    charPaneRect.height = getHeight();
    //}}}

    //first line to paint
    int lineStart = 0;
    //last line to paint
    int lineEnd = 1+(buffer.getLength()/bytesPerLine); //number of lines
    if(g2d.getClipBounds() != null) {
      lineStart = g2d.getClipBounds().y/charHeight;
      lineEnd = lineStart+g2d.getClipBounds().height/charHeight+1;
    }

    //make 9 charWidths over for spacing
    //paint background
    g2d.setPaint(getBackground());
    g2d.fillRect(0, 0, getWidth(), getHeight());
    //paint offset background
    g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, .2f));
    g2d.setPaint(new GradientPaint(0,0,getForeground(), charWidth*9,0,new Color(getForeground().getRed(), getForeground().getGreen(), getForeground().getBlue(), 128)));
    g2d.fillRect(0, 0, charWidth*9, getHeight());
    //paint unselectedPane color
    g2d.setPaint(getForeground());
    g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, .07f));
    if(getFocusedPane() == HEX_PANE)
      //paint char pane
      g2d.fill(charPaneRect);
    else
      //paint hex pane
      g2d.fill(hexPaneRect);
    g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1.0f));

    g2d.setPaint(getForeground());
    String offsetStr = "";
    int firstSelectedByte = -1; //inclusive
    int lastSelectedByte = -1;  //inclusive
    int loc = 0;
    int lineStartOffset = 0;
    //go until the end of lines that need painted OR the last line is reached
    for(int i=lineStart; (i<=lineEnd)&&(i<1+buffer.getLength()/bytesPerLine); i++) {
      lineStartOffset = getLineStartOffset(i);

      //{{{ draw selection
      if(getSelectionStart() <= getLineStartOffset(i))
        firstSelectedByte = 0;
      else if(getSelectionStart() >= getLineStartOffset(i+1))
        firstSelectedByte = -1;
      else
        firstSelectedByte = getSelectionStart()-getLineStartOffset(i);

      if(getSelectionEnd() >= getLineStartOffset(i+1))
        lastSelectedByte = getLineStartOffset(i+1)-getLineStartOffset(i);
      else if(getSelectionEnd() < getLineStartOffset(i))
        lastSelectedByte = -1;
      else
        lastSelectedByte = getSelectionEnd()-getLineStartOffset(i);

      if(firstSelectedByte!=-1 && lastSelectedByte!=-1) {
        g2d.setPaint(selectionColor);

        //draw selection in hex pane
        int startLoc = hexPaneRect.x+charWidth*(firstSelectedByte*2 + firstSelectedByte/grouping);
        int endLoc = hexPaneRect.x+charWidth*(lastSelectedByte*2 + lastSelectedByte/grouping);
        g2d.fillRect(startLoc, i*charHeight, endLoc-startLoc, charHeight);

        //draw selection in char pane
        startLoc = charPaneRect.x + charWidth*getDisplayableText(lineStartOffset, lineStartOffset+firstSelectedByte).length();
        endLoc = charPaneRect.x + charWidth*getDisplayableText(lineStartOffset, lineStartOffset+lastSelectedByte).length();
        g2d.fillRect(startLoc, i*charHeight, endLoc-startLoc, charHeight);
      }
      //}}}

      //draw carets if on screen
      g2d.setPaint(caretColor);
      if(getCaretLine()==i) {
        if(getFocusedPane()==CHARACTER_PANE || caretVisible) {
          Point hexPaneLoc = getLocationOfOffset(new OffsetInfo(HEX_PANE, caret, atHalfByte));
          g2d.drawRect(hexPaneLoc.x,hexPaneLoc.y,0,charHeight-1);
        }
        if(getFocusedPane()==HEX_PANE || caretVisible) {
          Point charPaneLoc = getLocationOfOffset(new OffsetInfo(CHARACTER_PANE, caret, atHalfByte));
          g2d.drawRect(charPaneLoc.x,charPaneLoc.y,0,charHeight-1);
        }
      }

      g2d.setPaint(offsetColumnColor);
      //draw offset
      offsetStr = Integer.toHexString(i*bytesPerLine);
      offsetStr = "0000000".substring(0,8-offsetStr.length()) + offsetStr;
      g2d.drawString(offsetStr, 0, ascent+i*charHeight);

      //{{{ draw hex columns
      loc = hexPaneRect.x;
      for(int j=lineStartOffset; j<lineStartOffset+bytesPerLine && j<buffer.getLength(); j++) {
        if(getSelectionStart()<=j && getSelectionEnd()>j) {  //if this byte is selected
          g2d.setPaint(selectedTextColor);
        } else {
          if((int)((j-lineStartOffset)/grouping%2) == 0)
            g2d.setPaint(oddColumnColor);
          else
            g2d.setPaint(evenColumnColor);
        }

        g2d.drawString(getHexString(j), loc, ascent+i*charHeight);

        if((j-lineStartOffset+1)%grouping != 0)
          loc += charWidth*2;
        else
          loc += charWidth*3;
      }
      //}}}

      //draw chars
      loc = charPaneRect.x;
      for(int j=lineStartOffset; j<lineStartOffset+bytesPerLine && j<buffer.getLength(); j++) {
        if(getSelectionStart()<=j && getSelectionEnd()>j) {  //if this byte is selected
          g2d.setPaint(selectedTextColor);
        } else {
          g2d.setPaint(textColumnColor);
        }

        g2d.drawString(getDisplayableText(j), loc, ascent+i*charHeight);

        loc += getDisplayableText(j).length() * charWidth;
      }

    }

    g2d.dispose();
  }
  //}}}

  class OffsetInfo {
    public OffsetInfo() {}
    public OffsetInfo(int pane, int offset, boolean atHalfByte) {
      this.pane = pane;
      this.offset = offset;
      this.atHalfByte = atHalfByte;
    }
    int pane = -1;
    int offset = -1;
    boolean atHalfByte;
  }

  //{{{ locationToOffset()
  /*
   * Won't return an offset larger than buffer.getLength()
   * Will return -1 if not in a pane.
   */
  //TODO: maybe (probably not) make cleaner?
  public OffsetInfo locationToOffset(Point location) {
    OffsetInfo info = new OffsetInfo();
    int firstOffset = getLineStartOffset(location.y/charHeight);
    if( hexPaneRect.contains(location) ) {
      info.pane = HEX_PANE;

      //find offset
      int columnWidth = (grouping*2+1) * charWidth;
      int column = (location.x - hexPaneRect.x) / columnWidth;
      int columnX = hexPaneRect.x + columnWidth * column;
      float j = 0.5f * charWidth;
      for(int i=1; i<=grouping*2; i++) {
        if(location.x < columnX + j) {
          info.offset = firstOffset + column*grouping + (i-1)/2;
          //if i is even, set atHalfByte to true
          if(i%2 == 0)
            info.atHalfByte = true;
          break;
        }
        if(i == grouping*2)
          info.offset = firstOffset + (column+1) * grouping;
        if(i == grouping*2-1)
          j += 1.5f * charWidth;
        else
          j += charWidth;
      }
    }
    if( charPaneRect.contains(location) ) {
      info.pane = CHARACTER_PANE;

      float j = 0;
      for(int i=0; i<=bytesPerLine; i++) {
        //width of all before it, and half of current
        if(i == bytesPerLine) {//if last time
          //offset should be beginning of next line
          info.offset = getLineStartOffset(location.y/charHeight+1);
        }
        if(firstOffset+i >= buffer.getLength()) {
          info.offset = buffer.getLength();
          break; //keep from passing to large value into getDisplayableText()
        }
        j = charWidth*getDisplayableText(firstOffset, firstOffset+i).length() + 0.5f*charWidth*getDisplayableText(firstOffset+i).length();
        if(location.x < charPaneRect.x+j) {
          info.offset = firstOffset+i;
          break;
        }
      }
    }

    if(info.offset >= buffer.getLength()) {
      info.offset = buffer.getLength();
      info.atHalfByte = false;
    }
    return info;
  }
  //}}}

  //{{{ KeyHandler
  //only basic editing keys are handled by the hex area..any with ctrl are not
  class KeyHandler extends KeyAdapter {
    public void keyPressed(KeyEvent e) {
      if(e.isConsumed() || e.isControlDown())
        return;
      switch(e.getKeyCode()) {
        //{{{ direction keys
        case KeyEvent.VK_LEFT: {
          e.consume();
          if(e.isShiftDown()) {
              if(getCaretPosition()-1 >=0 )
                moveCaretPosition(getCaretPosition()-1);
          } else {
            //if char pane is focused, should move full byte
            if(getFocusedPane() == CHARACTER_PANE) {
              if(getCaretPosition()-1 >=0 )
                setCaretPosition(getCaretPosition()-1, false);
            } else
            if(atHalfByte) {
              setCaretPosition(getCaretPosition(), false);    //set to false
            } else {
              if(getCaretPosition()-1 >=0 ) {
                setCaretPosition(getCaretPosition()-1, true); //set to true
              }
            }
          }
          scrollToCaret();
          break;
        }
        case KeyEvent.VK_RIGHT: {
          e.consume();
          if(e.isShiftDown()) {
            if(getCaretPosition() != buffer.getLength())
              moveCaretPosition(getCaretPosition()+1);
          } else {
            //if char pane is focused, should move full byte
            if(getFocusedPane() == CHARACTER_PANE) {
              if(getCaretPosition()+1 <= buffer.getLength())
                setCaretPosition(getCaretPosition()+1, false);
            } else
            if(atHalfByte) {
              //assert getCaretPosition()+1 <= buffer.getLength();
              setCaretPosition(getCaretPosition()+1, false); //set to false
            } else {
              if(getCaretPosition() != buffer.getLength())
                setCaretPosition(getCaretPosition(), true); //clear selection
            }
          }
          scrollToCaret();
          break;
        }
        case KeyEvent.VK_UP: {
          e.consume();
          if(e.isShiftDown()) {
            if(getCaretPosition()-bytesPerLine >= 0)
              moveCaretPosition(getCaretPosition()-bytesPerLine);
            else
              moveCaretPosition(0);
          } else {
            if(getCaretPosition()-bytesPerLine >= 0)
              setCaretPosition(getCaretPosition()-bytesPerLine);
            else
              setCaretPosition(0, false);
          }
          scrollToCaret();
          break;
        }
        case KeyEvent.VK_DOWN: {
          e.consume();
          if(e.isShiftDown()) {
            if(getCaretPosition()+bytesPerLine <= buffer.getLength())
              moveCaretPosition(getCaretPosition()+bytesPerLine);
            else
              moveCaretPosition(buffer.getLength());
          } else {
            if(getCaretPosition()+bytesPerLine < buffer.getLength())//less than
              setCaretPosition(getCaretPosition()+bytesPerLine);
            else
              setCaretPosition(buffer.getLength(), false);
          }
          scrollToCaret();
          break;
        }
        //}}}
        case KeyEvent.VK_PAGE_UP: {
          e.consume();
          int bytesToScroll = (getVisibleRect().height/charHeight-1)*bytesPerLine;
          if(e.isShiftDown()) {
            if(getCaretPosition()-bytesToScroll >= 0)
              moveCaretPosition(getCaretPosition()-bytesToScroll);
            else
              moveCaretPosition(0);
          } else {
            if(getCaretPosition()-bytesToScroll >= 0)
              setCaretPosition(getCaretPosition()-bytesToScroll);
            else
              setCaretPosition(0, false);
          }
          scrollToCaret();
          break;
        }
        case KeyEvent.VK_PAGE_DOWN: {
          e.consume();
          int bytesToScroll = (getVisibleRect().height/charHeight-1)*bytesPerLine;
          if(e.isShiftDown()) {
            if(getCaretPosition()+bytesToScroll <= buffer.getLength())
              moveCaretPosition(getCaretPosition()+bytesToScroll);
            else
              moveCaretPosition(buffer.getLength());
          } else {
            if(getCaretPosition()+bytesToScroll < buffer.getLength())//less than
              setCaretPosition(getCaretPosition()+bytesToScroll);
            else
              setCaretPosition(buffer.getLength(), false);
          }
          scrollToCaret();
          break;
        }
        case KeyEvent.VK_HOME: {
          e.consume();
          if(e.isShiftDown())
            moveCaretPosition( getLineStartOffset(getLineOfOffset(getCaretPosition())) );
          else
            setCaretPosition( getLineStartOffset(getLineOfOffset(getCaretPosition())), false);
          scrollToCaret();
          break;
        }
        case KeyEvent.VK_END: {
          e.consume();
          if(e.isShiftDown())
            moveCaretPosition( getLineEndOffset(getLineOfOffset(getCaretPosition())) );
          else
            setCaretPosition( getLineEndOffset(getLineOfOffset(getCaretPosition())), false);
          scrollToCaret();
          break;
        }
        case KeyEvent.VK_DELETE: {
          e.consume();
          delete();
          scrollToCaret();
          break;
        }
        case KeyEvent.VK_BACK_SPACE: {
          e.consume();
          backspace();
          scrollToCaret();
          break;
        }
        case KeyEvent.VK_INSERT: {
          setInsertEnabled(!isInsertEnabled());
          break;
        }
        case KeyEvent.VK_TAB: {
          e.consume();
          if(getFocusedPane() == HEX_PANE)
            setFocusedPane(CHARACTER_PANE);
          else
            setFocusedPane(HEX_PANE);
          break;
        }
        default: {
          if(e.getKeyChar() != KeyEvent.CHAR_UNDEFINED) {
            userInput(e.getKeyChar());
            scrollToCaret();
          }
          break;
        }
      }

    }
  }
  //}}}

  //{{{ MouseHandler
  class MouseHandler extends MouseInputAdapter {
    private boolean canDrag = false;
    public void mousePressed(MouseEvent e) {
      requestFocusInWindow();
      OffsetInfo info = locationToOffset(e.getPoint());
      if(info.pane==HEX_PANE || info.pane==CHARACTER_PANE) {
        canDrag = true;
        int oldPane = getFocusedPane();
        setFocusedPane(info.pane);
        if(e.isShiftDown())
            moveCaretPosition(info.offset);
        else
            setCaretPosition(info.offset, info.atHalfByte);
        if(oldPane != getFocusedPane())
          repaint();
      }
    }
    public void mouseReleased(MouseEvent e) {
      canDrag = false;
    }
    public void mouseDragged(MouseEvent e) {
      if(!canDrag)
        return;
      OffsetInfo info = locationToOffset(e.getPoint());
      if(info.pane == getFocusedPane()) {
        moveCaretPosition(info.offset);
      } else {
        if(e.getY() < 0)
          moveCaretPosition(0);
        else if(e.getY() >= getHeight())
          moveCaretPosition(buffer.getLength());
        else

        if(e.getX() < (getFocusedPane()==HEX_PANE ? hexPaneRect.x : charPaneRect.x))
          moveCaretPosition(getLineStartOffset(e.getY()/charHeight));
        else
          moveCaretPosition(Math.min( getLineStartOffset(e.getY()/charHeight+1), buffer.getLength() ));
      }
      scrollToCaret(); //TODO: scroll differently (without mouse moving)
    }
    public void mouseMoved(MouseEvent e) {
      if(e.getX() > hexPaneRect.x)
        setCursor(Cursor.getPredefinedCursor(Cursor.TEXT_CURSOR));
      else
        setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
    }
  }
  //}}}

  //{{{ FocusHandler
  class FocusHandler implements FocusListener {
    private ActionListener caretBlinker = new ActionListener() {
        public void actionPerformed(ActionEvent e) {
          caretVisible = !caretVisible;
          repaintLine(getCaretLine());
        }
      };
    public void focusGained(FocusEvent e) {
      caretTimer.addActionListener(caretBlinker);
      caretTimer.start();
    }
    public void focusLost(FocusEvent e) {
      caretTimer.stop();
      caretTimer.removeActionListener(caretBlinker);

      caretVisible = true;
      repaintLine(getCaretLine());
    }
  }
  //}}}

  //{{{ CaretListeners
  EventListenerList listenerList = new EventListenerList();
  CaretEvent caretEvent = null;

  public void addCaretListener(CaretListener l) {
    listenerList.add(CaretListener.class, l);
  }

  public void removeCaretListener(CaretListener l) {
    listenerList.remove(CaretListener.class, l);
  }

  protected void fireCaretUpdate() {
  // Guaranteed to return a non-null array
  Object[] listeners = listenerList.getListenerList();
  // Process the listeners last to first, notifying
  // those that are interested in this event
  for (int i = listeners.length-2; i>=0; i-=2) {
    if (listeners[i]==CaretListener.class) {
      if (caretEvent == null)
        caretEvent = new CaretEvent(this) {
          public int getDot() {
            return caret;
          }
          public int getMark() {
            return mark;
          }
        };
      ((CaretListener)listeners[i+1]).caretUpdate(caretEvent);
    }
  }
  }
  //}}}

  //{{{ Scrollable implementation
  public Dimension getPreferredScrollableViewportSize() {
    return getPreferredSize();
  }
  public int getScrollableUnitIncrement(Rectangle visibleRect, int orientation, int direction) {
    if(orientation==SwingConstants.VERTICAL) {
      //return height of a text line
      return charHeight;
    } else {
      return charWidth*2;
    }
  }
  public int getScrollableBlockIncrement(Rectangle visibleRect, int orientation, int direction) {
    if(orientation==SwingConstants.VERTICAL) {
      //return height of all fully visible text lines minus one
      return (visibleRect.height/charHeight-1)*charHeight;
    } else {
      return charWidth*20;
    }
  }
  public boolean getScrollableTracksViewportWidth() {
    if (getParent() instanceof JViewport) {
      return (((JViewport)getParent()).getWidth() > getPreferredSize().width);
    }
    return false;
  }
  public boolean getScrollableTracksViewportHeight() {
    if (getParent() instanceof JViewport) {
      return (((JViewport)getParent()).getHeight() > getPreferredSize().height);
    }
    return false;
  }
  //}}}
}
