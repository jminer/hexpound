/*
 * HexpoundWindow.java - The main window of Hexpound
 * Copyright (C) 2004, 2005 Jordan Miner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package hexpound;

import java.awt.*;
import java.awt.datatransfer.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.WindowConstants;

public class HexpoundWindow extends JFrame {
  private FindDialog findDialog;
  private DataViewer dataViewer;

  //private static HashMap actions = new HashMap();
  private static ArrayList hexAreas = new ArrayList();

  private static JMenuBar menuBar = new JMenuBar();
  private static JToolBar toolBar = new JToolBar();
  private static JTabbedPane tabbedPane = new JTabbedPane();
  private static StatusBar statusBar = new StatusBar();

  private static boolean toolBarShown = false;
  private static boolean statusBarShown = false;

  private static JMenu scriptsMenu = new JMenu("Scripts");

  //TODO: this isn't a selection listener, but without scripts changing the selection, the user can't tell the difference. Still, should probably make a selection listener
  private static CaretListener caretListener = new CaretListener() {
    public void caretUpdate(CaretEvent e) {
      StatusBar.update();
      DataViewer.update();
      Hexpound.updateOtherActionsEnabled();
    }
  };

  public HexpoundWindow() {

    setTitle("Hexpound");
    setIconImage(Toolkit.getDefaultToolkit().getImage( Hexpound.class.getResource("/icons/hexpound.png") ));

    //{{{ setup menus
    JMenu fileMenu = new JMenu("File");
      fileMenu.add(createMenuItem("new"));
      fileMenu.add(createMenuItem("open"));
      fileMenu.add(createMenuItem("save"));
      fileMenu.add(createMenuItem("save-as"));
      fileMenu.add(createMenuItem("close"));
      fileMenu.addSeparator();
      fileMenu.add(createMenuItem("exit"));
      menuBar.add(fileMenu);
    JMenu editMenu = new JMenu("Edit");
      editMenu.add(createMenuItem("cut"));
      editMenu.add(createMenuItem("copy"));
      editMenu.add(createMenuItem("copy-plain-text"));
      editMenu.add(createMenuItem("paste"));
      editMenu.add(createMenuItem("select-all"));
      editMenu.addSeparator();
      editMenu.add(createMenuItem("find"));
      editMenu.add(createMenuItem("find-next"));
      editMenu.add(createMenuItem("go-to-offset"));
      menuBar.add(editMenu);
    JMenu viewMenu = new JMenu("View");
      viewMenu.add(createMenuItem("toggle-tool-bar"));
      viewMenu.add(createMenuItem("toggle-status-bar"));
      viewMenu.add(createMenuItem("data-viewer"));
      viewMenu.addSeparator();
      viewMenu.add(createMenuItem("go-to-previous-tab"));
      viewMenu.add(createMenuItem("go-to-next-tab"));
      viewMenu.addSeparator();
      viewMenu.add(createMenuItem("scroll-to-caret"));
      menuBar.add(viewMenu);
    JMenu toolsMenu = new JMenu("Tools");
      toolsMenu.add(createMenuItem("load-table"));
      toolsMenu.add(createMenuItem("unload-table"));
      toolsMenu.addSeparator();
      toolsMenu.add(createMenuItem("eval"));
      toolsMenu.addSeparator();
      toolsMenu.add(createMenuItem("rescan-scripts"));
      scriptsMenu.setIcon(new ImageIcon(Hexpound.class.getResource("/icons/blank.png")));
      Hexpound.rescanScripts();
      toolsMenu.add(scriptsMenu);
      toolsMenu.addSeparator();
      toolsMenu.add(createMenuItem("options"));
      menuBar.add(toolsMenu);
    JMenu helpMenu = new JMenu("Help");
      helpMenu.add(createMenuItem("version-check"));
      helpMenu.add(createMenuItem("about"));
      menuBar.add(helpMenu);

    setJMenuBar(menuBar);
    //}}}

    //{{{ setup toolbar
    toolBar.setRollover(true);
    toolBar.add(createToolBarButton("new"));
    toolBar.add(createToolBarButton("open"));
    toolBar.add(createToolBarButton("save"));
    toolBar.addSeparator();
    toolBar.add(createToolBarButton("cut"));
    toolBar.add(createToolBarButton("copy"));
    toolBar.add(createToolBarButton("paste"));
    toolBar.addSeparator();
    toolBar.add(createToolBarButton("options"));
    //toolBar.addSeparator();
    //toolBar.add(createToolBarButton("help"));
    /*for(int i=0; true; i++) {
      if(toolBar.getComponentAtIndex(i) == null)
        break;
      toolBar.getComponentAtIndex(i).setFocusable(false);
    }*/
    //}}}

    tabbedPane.addChangeListener(new ChangeListener() {
      public void stateChanged(ChangeEvent e) {
        tabsChanged();
      }
    });
    tabbedPane.setTransferHandler(new FileTransferHandler());
    tabbedPane.setFocusable(false);
    if(Hexpound.getProperty("tab-placement").equals("top"))
      tabbedPane.setTabPlacement(JTabbedPane.TOP);
    else if(Hexpound.getProperty("tab-placement").equals("bottom"))
      tabbedPane.setTabPlacement(JTabbedPane.BOTTOM);
    else if(Hexpound.getProperty("tab-placement").equals("left"))
      tabbedPane.setTabPlacement(JTabbedPane.LEFT);
    else if(Hexpound.getProperty("tab-placement").equals("right"))
      tabbedPane.setTabPlacement(JTabbedPane.RIGHT);

    getContentPane().setLayout(new BorderLayout());
    setToolBarShown(Hexpound.getBooleanProperty("show-tool-bar"));
    //getContentPane().add(toolBar, BorderLayout.NORTH);
    getContentPane().add(tabbedPane, BorderLayout.CENTER);
    setStatusBarShown(Hexpound.getBooleanProperty("show-status-bar"));
    //getContentPane().add(statusBar, BorderLayout.SOUTH);

    setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
    addWindowListener(new WindowAdapter() {
      public void windowClosing(WindowEvent e) {
        Hexpound.exit();
      }
      public void windowActivated(WindowEvent e) {
        String tablesStr = "";
        for(int i = 0; i<Hexpound.getBufferCount(); i++) {
          Table t = Hexpound.getBuffer(i).getTable();
          //hasFileChanged() returns false if the file is null
          if(t != null && t.hasFileChanged()) {
            Hexpound.loadTable(t.getFile());
            tablesStr += t.getFile().getName() + "\n";
          }
        }
        if(tablesStr.length() != 0)
          JOptionPane.showMessageDialog(HexpoundWindow.this, "Tables automatically reloaded:\n\n"+tablesStr);
      }
    });

    setSize(Hexpound.getIntegerProperty("window.width", 640), Hexpound.getIntegerProperty("window.height", 480));
    //center on screen
    setLocationRelativeTo(null);

    addComponentListener(new ComponentAdapter() {
      public void componentResized(ComponentEvent e) {
        if((getExtendedState() & MAXIMIZED_BOTH) != 0) {
          Hexpound.setBooleanProperty("window.maximized", true);
        } else {
          Hexpound.setBooleanProperty("window.maximized", false);

          Hexpound.setIntegerProperty("window.width", getWidth());
          Hexpound.setIntegerProperty("window.height", getHeight());
        }
      }
    });
    if(Hexpound.getBooleanProperty("window.maximized"))
      setExtendedState(MAXIMIZED_BOTH);
  }
  private JMenuItem createMenuItem(String actionName) {
    if(Hexpound.getAction(actionName) == null) {
      System.out.println("problem: getAction\""+actionName+"\"returned null");
      return null;
    }

    JMenuItem menuItem = null;
    if(!Hexpound.getAction(actionName).isToggle())
      menuItem = new JMenuItem();
    else
      menuItem = new JCheckBoxMenuItem();
    new ActionBinder().bind(menuItem, Hexpound.getAction(actionName));
    return menuItem;
  }
  private AbstractButton createToolBarButton(String actionName) {
    if(Hexpound.getAction(actionName) == null) {
      System.out.println("problem: getAction\""+actionName+"\"returned null");
      return null;
    }

    AbstractButton button = null;
    if(!Hexpound.getAction(actionName).isToggle())
      button = new JButton();
    else
      button = new JToggleButton();
    new ActionBinder().bind(button, Hexpound.getAction(actionName));
    button.setFocusable(false);
    return button;
  }

  public JToolBar getToolBar() {
    return toolBar;
  }

  //{{{ setToolBar/StatusBarShown()
  public void setToolBarShown(boolean b) {
    if(b == toolBarShown) { //if no change needed
      return;
    } else {
      if(b)
        getContentPane().add(toolBar, BorderLayout.NORTH);
      else
        getContentPane().remove(toolBar);
      toolBarShown = !toolBarShown;
      validate();
    }
    Hexpound.setBooleanProperty("show-tool-bar", toolBarShown);
    Hexpound.getAction("toggle-tool-bar").setSelected(toolBarShown);
  }
  public boolean getToolBarShown() {
    return toolBarShown;
  }

  public void setStatusBarShown(boolean b) {
    if(b == statusBarShown) { //if no change needed
      return;
    } else {
      if(b)
        getContentPane().add(statusBar, BorderLayout.SOUTH);
      else
        getContentPane().remove(statusBar);
      statusBarShown = !statusBarShown;
      validate();
    }
    Hexpound.setBooleanProperty("show-status-bar", statusBarShown);
    Hexpound.getAction("toggle-status-bar").setSelected(toolBarShown);
  }
  public boolean getStatusBarShown() {
    return statusBarShown;
  }
  //}}}

  //placement must be "top" or "bottom" or "left" or "right"
  public void setTabPlacement(String placement) {
    if(placement.equals("top"))
      tabbedPane.setTabPlacement(JTabbedPane.TOP);
    else if(placement.equals("bottom"))
      tabbedPane.setTabPlacement(JTabbedPane.BOTTOM);
    else if(placement.equals("left"))
      tabbedPane.setTabPlacement(JTabbedPane.LEFT);
    else if(placement.equals("right"))
      tabbedPane.setTabPlacement(JTabbedPane.RIGHT);
    else
      throw new IllegalArgumentException("placement not valid");
  }
  private static void tabsChanged() {
    if(Hexpound.getCurrentBuffer() == null)
      Hexpound.getWindow().setTitle("Hexpound");
    else
      Hexpound.getWindow().setTitle("Hexpound - "+Hexpound.getCurrentBuffer().getName());
    StatusBar.update();
    DataViewer.update();
    Hexpound.updateOtherActionsEnabled();

    for(int i=0; i<hexAreas.size(); i++)
      getHexArea(i).removeCaretListener(caretListener);
    if(getHexAreaCount() > 0)
      getCurrentHexArea().addCaretListener(caretListener);
    focusHexArea();
  }
  static void addTab(Buffer buffer) {
    JHexArea hexArea = new JHexArea(buffer);
    hexAreas.add(hexArea);
    JScrollPane scrollPane = new JScrollPane(hexArea);
    scrollPane.setInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT, null);
    tabbedPane.addTab(buffer.getName(), scrollPane);
    tabbedPane.setSelectedIndex(tabbedPane.getTabCount()-1);
    if(buffer.getFile() != null)
      tabbedPane.setToolTipTextAt(tabbedPane.getSelectedIndex(), buffer.getFile().getAbsolutePath());
    focusHexArea();
  }
  static void removeTab(int index) {
    hexAreas.remove(index);
    tabbedPane.remove(index);
    //need this because Swing doesn't call stateChanged when a tab is deleted!
    tabsChanged();
    focusHexArea();
  }
  static void updateScriptsMenu(final File[] scriptFiles) {
    scriptsMenu.removeAll();
    JMenuItem item = null;
    String str = null;
    for(int i=0; i<scriptFiles.length; i++) {
      str = scriptFiles[i].getName().replace('_',' ');
      str = str.substring(0, str.lastIndexOf(".bsh"));
      item = new JMenuItem(str);
      final int j=i;
      item.addActionListener(new ActionListener() {
        File f = scriptFiles[j];
        public void actionPerformed(ActionEvent e) {
          Hexpound.executeScript(f);
        }
      });
      scriptsMenu.add(item);
    }
    scriptsMenu.revalidate();
  }
  public static int getSelectedTabIndex() {
    return tabbedPane.getSelectedIndex();
  }
  public static void setSelectedTabIndex(int index) {
    tabbedPane.setSelectedIndex(index);
  }
  static JTabbedPane getTabbedPane() {
    return tabbedPane;
  }
  public static int getHexAreaCount() {
    return hexAreas.size();
  }
  public static JHexArea getHexArea(int index) {
    return (JHexArea)hexAreas.get(index);
  }
  public static JHexArea getCurrentHexArea() {
    if(hexAreas.size() < 1)
      return null;
    return (JHexArea)hexAreas.get(tabbedPane.getSelectedIndex());
  }

  public static void focusHexArea() {
    EventQueue.invokeLater(new Runnable() {
      public void run() {
        if(getCurrentHexArea() != null)
          getCurrentHexArea().requestFocusInWindow();
      }
    });
  }

}
