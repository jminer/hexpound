/*
 * AdvancedAction.java - An action that holds scripted code
 * Copyright (C) 2005 Jordan Miner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package hexpound;

import bsh.EvalError;
import bsh.Interpreter;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.EventListenerList;
import javax.swing.ImageIcon;
import javax.swing.KeyStroke;

public class AdvancedAction {
  private String name;
  private String code = "";
  private KeyStroke accelerator;

  private String label = "";
  // All actions by default have a blank icon, rather than none, in order to cause the
  // menu items to have a consistent height.
  private ImageIcon icon = new ImageIcon(Hexpound.class.getResource("/icons/blank.png"));
  private ImageIcon rolloverIcon;
  private boolean enabled = true;
  private boolean isToggle = false;
  private boolean selected = false;

  public AdvancedAction(String name) {
    setName(name);
  }

  //{{{ package private methods
  void setCode(String newCode) {
    if(newCode == null)
      code = "";
    else
      code = newCode;
    fireStateChanged();
  }
  void setName(String name) {
    if(name == null)
      throw new NullPointerException("AdvancedAction: name must not be null");
    this.name = name;
    fireStateChanged();
  }
  //}}}

  public void invoke() {
    try {
      Interpreter bsh = new Interpreter();
      bsh.eval("import hexpound.*; ");
      bsh.set("window", Hexpound.getWindow());
      bsh.set("buffer", Hexpound.getCurrentBuffer());
      bsh.set("hexArea", Hexpound.getWindow().getCurrentHexArea());
      bsh.eval(code);
    } catch(EvalError e) {
      new ExceptionDialog(Hexpound.getWindow(), "Action exception:", "Error", e.toString()).setVisible(true);
    }
  }
  public String getName() {
    return name;
  }
  public String getCode() {
    return code;
  }

  public String getLabel() {
    return label;
  }
  public void setLabel(String s) {
    if(s == null)
      throw new NullPointerException("s must not be null");
    label = s;
    fireStateChanged();
  }

  public ImageIcon getIcon() {
    return icon;
  }
  public void setIcon(ImageIcon i) {
    icon = i;
    fireStateChanged();
  }
  //will return the normal icon if a rollover one is not set
  public ImageIcon getRolloverIcon() {
    if(rolloverIcon == null)
      return icon;
    else
      return rolloverIcon;
  }
  public void setRolloverIcon(ImageIcon i) {
    rolloverIcon = i;
    fireStateChanged();
  }

  public String getToolTipText() {
    String accelStr = getAcceleratorString();
    if(accelStr == null)
      return getLabel();
    else
      return getLabel()+" ("+accelStr+")";
  }

  //{{{ accelerator
  //if s is null, accelerator will be null
  public void setAccelerator(String s) {
    accelerator = KeyStroke.getKeyStroke(s);
    fireStateChanged();
  }
  public void setAccelerator(KeyStroke ks) {
    accelerator = ks;
    fireStateChanged();
  }
  public KeyStroke getAccelerator() {
    return accelerator;
  }
  //returns a String like "Ctrl+Shift+C"
  public String getAcceleratorString() {
    if(accelerator == null)
      return null;
    return KeyEvent.getKeyModifiersText(accelerator.getModifiers()) + "+" + KeyEvent.getKeyText(accelerator.getKeyCode());
  }
  //}}}

  public boolean isEnabled() {
    return enabled;
  }
  public void setEnabled(boolean b) {
    boolean shouldFire = enabled != b;
    enabled = b;
    if(shouldFire)
      fireStateChanged();
  }

  public boolean isToggle() {
    return isToggle; //only return true if it is a toggle action
  }
  public void setToggle(boolean b) {
    boolean shouldFire = isToggle != b;
    isToggle = b;
    if(shouldFire)
      fireStateChanged();
  }

  public boolean isSelected() {
    return selected && isToggle; //only return true if it is a toggle action
  }
  public void setSelected(boolean b) {
    if(!isToggle) return;   //can't be selected if it isn't toggle
    boolean shouldFire = selected != b;
    selected = b;
    if(shouldFire)
      fireStateChanged();
  }

  //{{{ change listeners
  EventListenerList listenerList = new EventListenerList();
  ChangeEvent changeEvent = null;

  public void addChangeListener(ChangeListener l) {
     listenerList.add(ChangeListener.class, l);
  }

  public void removeChangeListener(ChangeListener l) {
     listenerList.remove(ChangeListener.class, l);
  }

  // Notify all listeners that have registered interest for
  // notification on this event type.  The event instance
  // is lazily created using the parameters passed into
  // the fire method.
  protected void fireStateChanged() {
    // Guaranteed to return a non-null array
    Object[] listeners = listenerList.getListenerList();
    // Process the listeners last to first, notifying
    // those that are interested in this event
    for (int i = listeners.length-2; i>=0; i-=2) {
      if (listeners[i]==ChangeListener.class) {
        // Lazily create the event:
        if (changeEvent == null)
          changeEvent = new ChangeEvent(this);
        ((ChangeListener)listeners[i+1]).stateChanged(changeEvent);
      }
    }
  }
  //}}}

  public String toString() {
    return "AdvancedAction: "+name+"[label="+label+", accelerator="+getAcceleratorString()+"]";
  }
}
