/*
 * FindDialog.java - Hexpound's find dialog
 * Copyright (C) 2004, 2005 Jordan Miner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package hexpound;

import java.awt.Cursor;
import java.awt.event.*;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

//if table loaded, can't search by value
public class FindDialog extends EnhancedDialog {
  private static FindDialog instance;

  private JTextField textField;
  private JRadioButton textValue;
  private JRadioButton hexValue;

  private JCheckBox searchRelative;

  private JRadioButton searchBackward;
  private JRadioButton searchForward;

  private JCheckBox useRange;
  private JTextField startRange;
  private JTextField endRange;
  private JButton findButton;
  private JButton closeButton;

  public FindDialog() {
    super(Hexpound.getWindow(), "Find", false);

    GridBagLayout gb = new GridBagLayout();
    GridBagConstraints c = new GridBagConstraints();
    JPanel panel = new JPanel(gb);
    panel.setBorder(new EmptyBorder(5,5,5,5));
    getContentPane().add(panel);
    c.fill = GridBagConstraints.HORIZONTAL;
    c.anchor = GridBagConstraints.PAGE_START;

    textField = new JTextField(10);
    c.gridx = 0;
    c.gridy = 0;
    c.gridwidth = 2;
    c.weightx = 0.5;
    c.insets = new Insets(5,5,5,5);
    gb.setConstraints(textField, c);
    panel.add(textField);
    c.weightx = 0;

    JPanel inputTypePanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
    inputTypePanel.setBorder(new TitledBorder("Input Type"));
    ButtonGroup inputTypeGroup = new ButtonGroup();
    textValue = new JRadioButton("Text", true);
    inputTypeGroup.add(textValue);
    inputTypePanel.add(textValue);
    hexValue = new JRadioButton("Hex", false);
    inputTypeGroup.add(hexValue);
    inputTypePanel.add(hexValue);
    c.gridx = 0;
    c.gridy = 1;
    c.gridwidth = 1;
    c.gridheight = 1;
    gb.setConstraints(inputTypePanel, c);
    panel.add(inputTypePanel);

    searchRelative = new JCheckBox("Relative");
    c.gridx = 0;
    c.gridy = 2;
    c.gridwidth = 1;
    c.gridheight = 1;
    c.insets = new Insets(0,0,0,0);
    gb.setConstraints(searchRelative, c);
    panel.add(searchRelative);

    JPanel directionPanel = new JPanel();
    directionPanel.setLayout(new BoxLayout(directionPanel, BoxLayout.Y_AXIS));
    directionPanel.setBorder(new TitledBorder("Direction"));
    ButtonGroup searchGroup = new ButtonGroup();
    searchBackward = new JRadioButton("Backward", false);
    searchGroup.add(searchBackward);
    searchForward = new JRadioButton("Forward", true);
    searchGroup.add(searchForward);
    directionPanel.add(searchBackward);
    directionPanel.add(searchForward);
    c.gridx = 1;
    c.gridy = 1;
    c.gridwidth = 1;
    c.gridheight = 2;
    gb.setConstraints(directionPanel, c);
    panel.add(directionPanel);

    JPanel rangePanel = new JPanel(new GridLayout(3, 2, 2, 2));
    rangePanel.setBorder(new TitledBorder("Range"));
    useRange = new JCheckBox("Use range");
    useRange.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        startRange.setEnabled(useRange.isSelected());
        endRange.setEnabled(useRange.isSelected());
      }
    });
    rangePanel.add(useRange);
    rangePanel.add(new JPanel());

    rangePanel.add(new JLabel("Start index: "));
    startRange = new JTextField(8);
    startRange.setEnabled(false);
    rangePanel.add(startRange);

    rangePanel.add(new JLabel("End index: "));
    endRange = new JTextField(8);
    endRange.setEnabled(false);
    rangePanel.add(endRange);
    c.gridx = 0;
    c.gridy = 3;
    c.gridwidth = 2;
    c.gridheight = 1;
    gb.setConstraints(rangePanel, c);
    panel.add(rangePanel);

    findButton = new JButton("Find");
    findButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        ok();
      }
    });
    c.gridx = 2;
    c.gridy = 0;
    c.gridwidth = 1;
    c.gridheight = 1;
    c.insets = new Insets(5,5,5,5);
    gb.setConstraints(findButton, c);
    panel.add(findButton);

    closeButton = new JButton("Close");
    closeButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        cancel();
      }
    });
    c.gridx = 2;
    c.gridy = 1;
    c.insets = new Insets(5,5,5,5);
    gb.setConstraints(closeButton, c);
    panel.add(closeButton);

    getRootPane().setDefaultButton(findButton);
    pack();
    setLocationRelativeTo(Hexpound.getWindow());
  }

  public static FindDialog getDialog() {
    if(instance == null)
      instance = new FindDialog();
    return instance;
  }

  //{{{ findText()
  public static Match findText(Buffer buf, String searchStr, int fromIndex, boolean reverse) {
    if(buf == null || searchStr == null)
      return null;
    Table table = buf.getTable();
    if(table == null)
      table = new Table();

    Match match = new Match();
    if(reverse) {
      //{{{ search backward
      //go over every byte reverse
main: for(int i=fromIndex; i >= 0; i--) {
        //j is index in String, k is times
        int k=0;
        for(int j=0; j<searchStr.length(); k++) {
          String tmp = table.convert(buf.getByteAt(i-k));
          if( !searchStr.substring(0, searchStr.length()-j).endsWith(tmp) )
            continue main;
          j += tmp.length();
        }
        match.end = i+1;
        match.start = match.end-k; //k - bytes to select
        return match;
      }
      //}}}
    } else {
      //{{{ search forward
      //go over every byte
main: for(int i=fromIndex; i<buf.getLength()-searchStr.length(); i++) {
        //j is index in String, k is times
        int k=0;
        for(int j=0; j<searchStr.length(); k++) {
          String tmp = table.convert(buf.getByteAt(i+k));
          if( !searchStr.substring(j).startsWith(tmp) )
            continue main;
          j += tmp.length();
        }
        match.start = i;
        match.end = match.start+k; //k - bytes to select
        return match;
      }
      //}}}
    }
    return null;
  }
  //}}}

  //{{{ findBytes()
  public static Match findBytes(Buffer buf, byte[] searchBytes, int fromIndex, boolean reverse) {
    if(buf == null || searchBytes == null)
      return null;

    Match match = new Match();
    if(reverse) {
      //{{{ search backward
main: for(int i=fromIndex-searchBytes.length; i>=0; i--) {
        for(int j=0; j < searchBytes.length; j++) {
          if(buf.getByteAt(i+j) != searchBytes[j])
            continue main;
        }
        match.start = i;
        match.end = match.start + searchBytes.length;
        return match;
      }
      //}}}
    } else {
      //{{{ search forward
                 //subtract searchBytes.length so i+j won't be past end of file
main: for(int i=fromIndex; i<buf.getLength()-searchBytes.length; i++) {
        for(int j=0; j < searchBytes.length; j++) {
          if(buf.getByteAt(i+j) != searchBytes[j])
            continue main;
        }
        match.start = i;
        match.end = match.start + searchBytes.length;
        return match;
      }
      //}}}
    }
    return null;
  }
  //}}}

  //{{{ findRelative()
  public static Match findRelative(Buffer buf, byte[] searchBytes, int fromIndex, boolean reverse) {
    if(buf == null || searchBytes == null)
      return null;

    int[] byteDiff = new int[searchBytes.length];
    for(int i=0; i<byteDiff.length; i++) {
      byteDiff[i] = searchBytes[i] - searchBytes[0];
    }

    Match match = new Match();
    if(reverse) {
      //{{{ search backward
main: for(int i=fromIndex-byteDiff.length; i>=0; i--) {
        for(int j=0; j<byteDiff.length; j++) {
          if(buf.getByteAt(i+j)-buf.getByteAt(i) != byteDiff[j])
            continue main;
        }
        match.start = i;
        match.end = match.start+byteDiff.length;
        return match;
      }
      //}}}
    } else {
      //{{{ search forward
main: for(int i=fromIndex; i<buf.getLength()-byteDiff.length; i++) {
        for(int j=0; j<byteDiff.length; j++) {
          if(buf.getByteAt(i+j)-buf.getByteAt(i) != byteDiff[j])
            continue main;
        }
        match.start = i;
        match.end = match.start+byteDiff.length;
        return match;
      }
      //}}}
    }
    return null;
  }
  //}}}

  //{{{ findBytes()
  public static Match findBytes(Buffer buf, byte[] searchBytes, boolean[] areBlank, int fromIndex, boolean reverse) {
    //TODO: check if areBlank is correct size
    if(buf == null || searchBytes == null)
      return null;

    Match match = new Match();
    if(reverse) {
      //{{{ search backward
main: for(int i=fromIndex-searchBytes.length; i>=0; i--) {
        for(int j=0; j < searchBytes.length; j++) {
          if(!areBlank[j] && buf.getByteAt(i+j) != searchBytes[j])
            continue main;
        }
        match.start = i;
        match.end = match.start + searchBytes.length;
        return match;
      }
      //}}}
    } else {
      //{{{ search forward
                 //subtract searchBytes.length so i+j won't be past end of file
main: for(int i=fromIndex; i<buf.getLength()-searchBytes.length; i++) {
        for(int j=0; j < searchBytes.length; j++) {
          if(!areBlank[j] && buf.getByteAt(i+j) != searchBytes[j])
            continue main;
        }
        match.start = i;
        match.end = match.start + searchBytes.length;
        return match;
      }
      //}}}
    }
    return null;
  }
  //}}}

  //{{{ findRelative()
  public static Match findRelative(Buffer buf, byte[] searchBytes, boolean[] areBlank, int fromIndex, boolean reverse) {
    if(buf == null || searchBytes == null)
      return null;

    int[] byteDiff = new int[searchBytes.length];
    for(int i=0; i<byteDiff.length; i++) {
      byteDiff[i] = searchBytes[i] - searchBytes[0];
    }

    Match match = new Match();
    if(reverse) {
      //{{{ search backward
main: for(int i=fromIndex-byteDiff.length; i>=0; i--) {
        for(int j=0; j<byteDiff.length; j++) {
          if(!areBlank[j] && buf.getByteAt(i+j)-buf.getByteAt(i) != byteDiff[j])
            continue main;
        }
        match.start = i;
        match.end = match.start+byteDiff.length;
        return match;
      }
      //}}}
    } else {
      //{{{ search forward
main: for(int i=fromIndex; i<buf.getLength()-byteDiff.length; i++) {
        for(int j=0; j<byteDiff.length; j++) {
          if(!areBlank[j] && buf.getByteAt(i+j)-buf.getByteAt(i) != byteDiff[j])
            continue main;
        }
        match.start = i;
        match.end = match.start+byteDiff.length;
        return match;
      }
      //}}}
    }
    return null;
  }
  //}}}

  //{{{ find()
  /* Searches with the settings in the find dialog from the specified index. */
  private FindDialog.Match find(int start) {
    //Don't need null check, because this is private
    Buffer buffer = Hexpound.getCurrentBuffer();
    String text = textField.getText();
    FindDialog.Match match = null;

    if(textValue.isSelected()) { //search text
      if(searchRelative.isSelected()) {
        match = findRelative(buffer, text.getBytes(), start, searchBackward.isSelected());
      } else {
        match = findText(buffer, text, start, searchBackward.isSelected());
      }
    } else { //search hex
      text = Hexpound.removeWhitespace(text);
      try {
        //{{{find the number of bytes that are being searched for
        int length = 0; //total length of byte array to make
        int index = 0;
        while(index < text.length()) {
          if(text.charAt(index)=='(') {
            if(index+3 > text.length())
              throw new NumberFormatException("incomplete blank byte");
            int closingParIndex = text.indexOf(')', index+1);
            if(closingParIndex == -1)
              throw new NumberFormatException("no closing parentheses");
            length += Integer.parseInt(text.substring(index+1, closingParIndex));
            index = closingParIndex+1;
          } else {
            if(index+2 > text.length())
              throw new NumberFormatException("only half of byte");
            length++;
            index += 2;
          }
        }
        //}}}

        //{{{ make arrays
        byte[] bytes = new byte[length];
        boolean[] areBlank = new boolean[length];
        for(int i=0; i<areBlank.length; i++)
          areBlank[i] = true;
        index = 0;
        for(int i=0; i<bytes.length; ) {
          if(text.charAt(index)=='(') {
            i += Integer.parseInt(text.substring(index+1, text.indexOf(')', index+1)));
            index = text.indexOf(')', index+1)+1;
          } else {
            areBlank[i] = false;
            bytes[i] = (byte)Integer.parseInt(text.substring(index, index+2), 16);
            i++;
            index += 2;
          }
        }
        //}}}

        if(searchRelative.isSelected()) {
          match = findRelative(buffer, bytes, areBlank, start, searchBackward.isSelected());
        } else {
          match = findBytes(buffer, bytes, areBlank, start, searchBackward.isSelected());
        }
      } catch(NumberFormatException e) {
        JOptionPane.showMessageDialog(this, "Invalid hex input.", "Error", JOptionPane.ERROR_MESSAGE);
        return null;
      }
    }
    return match;
  }
  //}}}

  //{{{ find()
  public void find() {
    Buffer buffer = Hexpound.getCurrentBuffer();
    if(buffer == null)
      return;
    int start = 0;
    int end = buffer.getLength();
    try {
      if(useRange.isSelected()) {
        start = Hexpound.decodeInt(startRange.getText());
        end = Hexpound.decodeInt(endRange.getText());
        if(start < 0 || start > end || end > buffer.getLength())
          throw new NumberFormatException();
      }
    } catch(NumberFormatException ex) {
      JOptionPane.showMessageDialog(this, "Invalid range.", "Error", JOptionPane.ERROR_MESSAGE);
      return;
    }
    int caret = HexpoundWindow.getCurrentHexArea().getCaretPosition();
    String wrapLocStr = null;
    FindDialog.Match match = null;
    if(searchForward.isSelected()) {
      match = find( Math.max(start, caret) );
      wrapLocStr = "beginning";
    } else {
      match = find( Math.min(end, caret) );
      wrapLocStr = "end";
    }

    if(match == null || match.start < start || match.end > end) {
      int wrap = JOptionPane.showConfirmDialog(this, "No more matches were found.\nWould you like to start searching from the "+wrapLocStr+"?", "Hexpound", JOptionPane.YES_NO_OPTION);
      if(wrap == JOptionPane.NO_OPTION)
        return;
      if(searchForward.isSelected())
        match = find(start);
      else
        match = find(end);
      if(match == null || match.start < start || match.end > end) {
        JOptionPane.showMessageDialog(this, "The input you entered was not found.", "Hexpound", JOptionPane.INFORMATION_MESSAGE);
        return;
      }
    }

    if(searchForward.isSelected()) {
      HexpoundWindow.getCurrentHexArea().setCaretPosition(match.start);
      HexpoundWindow.getCurrentHexArea().moveCaretPosition(match.end);
    } else {
      HexpoundWindow.getCurrentHexArea().setCaretPosition(match.end);
      HexpoundWindow.getCurrentHexArea().moveCaretPosition(match.start);
    }
    HexpoundWindow.getCurrentHexArea().scrollToCaret();
  }
  //}}}

  //{{{ ok()
  public void ok() {
    setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
    find();
    setCursor(Cursor.getDefaultCursor());
  }
  //}}}

  public void cancel() {
    setVisible(false);
  }

  public void show() {
    if(Hexpound.getCurrentBuffer() == null)
      return;
    textField.selectAll();
    textField.requestFocusInWindow();

    useRange.setSelected(HexpoundWindow.getCurrentHexArea().hasSelection());
    startRange.setEnabled(HexpoundWindow.getCurrentHexArea().hasSelection());
    endRange.setEnabled(HexpoundWindow.getCurrentHexArea().hasSelection());

    startRange.setText("0x"+Integer.toHexString( HexpoundWindow.getCurrentHexArea().getSelectionStart() ).toUpperCase());
    endRange.setText("0x"+Integer.toHexString( HexpoundWindow.getCurrentHexArea().getSelectionEnd() ).toUpperCase());
    super.show();
  }
  //{{{ Match class
  /**
   * Holds the information from a search. This is needed because the length
   * of the search string may not equal the number of matching bytes.
   */
  public static class Match {
    public int start = -1;
    public int end = -1;
    public String toString() {
      return "Match["+"start="+start+", end="+end+"]";
    }
  }
  //}}}
}
