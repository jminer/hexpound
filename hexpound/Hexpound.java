/*
 * Hexpound.java - The main class of Hexpound
 * Copyright (C) 2004, 2005 Jordan Miner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package hexpound;

import bsh.*;
import java.awt.Color;
import java.awt.EventQueue;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.helpers.XMLReaderFactory;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

/**
 * Created on Sunday, January 11, 2004, 11:48 AM.
 */
public class Hexpound {
  public static final String appDir = findAppDir();
  public static final String userDataDir = findUserDataDir();
  private static Properties props = new Properties();
  private static HashMap actions = new HashMap();
  private static ArrayList buffers = new ArrayList();
  private static Timer autoUpdateTimer;
  private static HexpoundWindow window;
  public static HexpoundWindow getWindow() {
    return window;
  }

  //{{{ readFile()
  public static String[] readFile(File file) throws IOException {
    return readFile(file, System.getProperty("file.encoding"));
  }
  //TODO: move to utilites and make autodetect()
  public static String[] readFile(File file, String encoding) throws IOException {
    InputStream in = new BufferedInputStream(new FileInputStream(file));
    in.mark(3);
    //{{{ check for unicode
    int b1 = in.read();
    int b2 = in.read();
    in.reset();
    if((b1 == 0xff && b2 == 0xfe) || (b1 == 0xfe && b2 == 0xff)) {
      encoding = "UTF-16";
    }
    //}}}
    BufferedReader reader = new BufferedReader(new InputStreamReader(in, encoding));
    ArrayList lineList = new ArrayList();
    String line = null;
    while((line = reader.readLine()) != null) {
      lineList.add(line);
    }
    String[] lines = new String[lineList.size()];
    for(int i=0; i<lineList.size(); i++) {
      lines[i] = (String)lineList.get(i);
    }
    return lines;
  }
  //}}}

  //{{{ Settings
  private static void loadProperties() {
    if(!new File(userDataDir + File.separator + "properties.txt").exists())
      return;
    InputStream in = null;
    try {
      in = new FileInputStream(userDataDir + File.separator + "properties.txt");
      props.load(in);
      in.close();
    } catch(IOException e) {
      System.out.println("IOException while reading properties.txt file: "+e.toString());
      JOptionPane.showMessageDialog(null, "IO error while reading properties file.", "Hexpound", JOptionPane.ERROR_MESSAGE);
    } catch(IllegalArgumentException e) {
      System.out.println("IllegalArgumentException while reading properties.txt file: "+e.toString());
      JOptionPane.showMessageDialog(null, "Error in properties file.", "Hexpound", JOptionPane.ERROR_MESSAGE);
    }
  }

  public static void saveProperties() {
    OutputStream out = null;
    try {
      out = new FileOutputStream(userDataDir + File.separator + "properties.txt");
      props.store(out, "This is the Hexpound properties file.");
      out.close();
    } catch(IOException e) {
      System.out.println("IOException while writing properties.txt file: "+e.toString());
      JOptionPane.showMessageDialog(null, "IO error while writing properties file.", "Hexpound", JOptionPane.ERROR_MESSAGE);
    }
  }

  //{{{ property getters
  public static final String getProperty(String name, String def) {
    String prop = props.getProperty(name);
    if(prop == null)
      return def;
    return prop;
  }
  public static final boolean getBooleanProperty(String name, boolean def) {
    String prop = getProperty(name, null);
    if(prop == null)
      return def;
    return prop.equals("true") || prop.equals("yes") || prop.equals("on");
  }
  //supports 0,0,255
  //supports #0000ff
  public static final Color getColorProperty(String name, Color def) {
    String prop = getProperty(name, null);
    if(prop == null)
      return def;
    try {
      if(prop.startsWith("#"))
        return Color.decode(prop);

      String[] colors = prop.split(",");
      if(colors.length < 3)
        return def;
      return new Color(Integer.parseInt(colors[0]), Integer.parseInt(colors[1]), Integer.parseInt(colors[2]));
    } catch(NumberFormatException e) {
      return def;
    }
  }
  public static final int getIntegerProperty(String name, int def) {
    String prop = getProperty(name, null);
    if(prop == null)
      return def;
    try {
      return Integer.parseInt(prop);
    } catch(NumberFormatException e) {
      return def;
    }
  }
  public static final String getProperty(String name) {
    return getProperty(name, "");
  }
  public static final boolean getBooleanProperty(String name) {
    return getBooleanProperty(name, false);
  }
  public static final Color getColorProperty(String name) {
    return getColorProperty(name, Color.BLACK);
  }
  public static final int getIntegerProperty(String name) {
    return getIntegerProperty(name, 0);
  }
  //}}}

  public static final void removeProperty(String name) {
    props.remove(name);
  }
  public static final void setProperty(String name, String value) {
    props.setProperty(name, value);
  }
  public static final void setBooleanProperty(String name, boolean value) {
    props.setProperty(name, String.valueOf(value));
  }
  public static final void setColorProperty(String name, Color value) {
    props.setProperty(name, value.getRed()+","+value.getGreen()+","+value.getBlue());
  }
  public static final void setIntegerProperty(String name, int value) {
    props.setProperty(name, String.valueOf(value));
  }
  //}}}

  //{{{ loadActions()
  private static void loadActions() {
    try {
      URL url = Hexpound.class.getResource("/actions.xml");
      if(url == null)
        throw new IOException();
      InputSource source = new InputSource(url.openStream());
      //TODO: uncomment these when dropping support for 1.4
      //XMLReader reader = XMLReaderFactory.createXMLReader();
      //reader.setContentHandler(new ActionsContentHandler());
      //reader.parse(source);
      SAXParser reader = SAXParserFactory.newInstance().newSAXParser();
      reader.parse(source, new ActionsContentHandler());
    } catch(IOException e) {
      e.printStackTrace();
      JOptionPane.showMessageDialog(null, "Error reading actions.xml file.\n"+"IOException: "+e.getMessage(), "Hexpound", JOptionPane.ERROR_MESSAGE);
      System.exit(1);
    } catch(SAXException e) {
      e.printStackTrace();
      JOptionPane.showMessageDialog(null, "Error parsing actions.xml file.\n"+"SAXException: "+e.getMessage(), "Hexpound", JOptionPane.ERROR_MESSAGE);
      System.exit(1);
    } catch(ParserConfigurationException e) { //TODO: delete when dropping 1.4
      e.printStackTrace();
      JOptionPane.showMessageDialog(null, "Could not find parser for actions.xml file.\n"+"ParserConfigurationException: "+e.getMessage(), "Hexpound", JOptionPane.ERROR_MESSAGE);
      System.exit(1);
    }
  }
  //}}}
  //{{{ ActionsContentHandler class
  private static class ActionsContentHandler extends DefaultHandler {
    private AdvancedAction currentAction;

    private StringBuffer strBuff = new StringBuffer();
    private boolean inTag = false;
    private boolean inLabelTag = false;
    private boolean inAcceleratorTag = false;
    private boolean inIconTag = false;
    private boolean inCodeTag = false;

    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
      localName = qName;  //TODO: delete when dropping 1.4
      if(localName.equals("action"))
        currentAction = new AdvancedAction(attributes.getValue("name"));
        //TODO: start using this again when dropping 1.4
        //currentAction = new AdvancedAction(attributes.getValue(uri, "name"));
      else if(localName.equals("label") || localName.equals("accelerator") || localName.equals("icon") || localName.equals("is_toggle") || localName.equals("code"))
        inTag = true;
    }
    public void endElement(String uri, String localName, String qName) throws SAXException {
      localName = qName;  //TODO: delete when dropping 1.4
      if(localName.equals("action")) {
        actions.put(currentAction.getName(), currentAction);
        currentAction = null;
      }
      if(localName.equals("label")) {
        inTag = false;
        currentAction.setLabel(strBuff.toString().trim());
        strBuff.setLength(0);
      } else if(localName.equals("accelerator")) {
        inTag = false;
        currentAction.setAccelerator(strBuff.toString().trim());
        strBuff.setLength(0);
      } else if(localName.equals("icon")) {
        inTag = false;
        URL url = Hexpound.class.getResource(strBuff.insert(0, "/icons/").toString().trim());
        if(url == null) {
          System.out.println("icon not found: "+strBuff.toString());
          return;
        }
        currentAction.setIcon(new ImageIcon(url));
        strBuff.setLength(0);
      } else if(localName.equals("is_toggle")) {
        inTag = false;
        currentAction.setToggle(strBuff.toString().trim().equals("true"));
        strBuff.setLength(0);
      } else if(localName.equals("code")) {
        inTag = false;
        currentAction.setCode(strBuff.toString().trim());
        strBuff.setLength(0);
      }
    }
    /* for some reason, the SAX parser will sometimes give you the text between tags in pieces...like 'Sa' and 've As...' instead of 'Save As...'*/
    public void characters(char[] ch, int start, int length) throws SAXException {
      if(inTag)
        strBuff.append(ch, start, length);
    }
  }
  //}}}

  private static void setDefaultProperties() {
    setIntegerProperty("bytes-per-line", getIntegerProperty("bytes-per-line", 16));
    setIntegerProperty("grouping", getIntegerProperty("grouping", 2));
    setIntegerProperty("font-size", getIntegerProperty("font-size", 15));
    setBooleanProperty("show-only-table-chars", getBooleanProperty("show-only-table-chars", true));
    setBooleanProperty("smooth-text", getBooleanProperty("smooth-text", false));
    setProperty("unknown-char", getProperty("unknown-char", "."));
    setProperty("look-and-feel", getProperty("look-and-feel", UIManager.getSystemLookAndFeelClassName()));
    setBooleanProperty("show-tool-bar", getBooleanProperty("show-tool-bar", true));
    setBooleanProperty("show-status-bar", getBooleanProperty("show-status-bar", true));
    setBooleanProperty("decorate-frames", getBooleanProperty("decorate-frames", false));
    setBooleanProperty("decorate-dialogs", getBooleanProperty("decorate-dialogs", false));
    setProperty("tab-placement", getProperty("tab-placement", "top"));
    setIntegerProperty("background-image.alpha", getIntegerProperty("background-image.alpha", 255));
  }
  private static void setDefaultColorProperties() {
    setColorProperty("offset-column-color", getColorProperty("offset-column-color", UIManager.getColor("TextArea.foreground")));
    setColorProperty("odd-column-color", getColorProperty("odd-column-color", new Color(0,0,255)));
    setColorProperty("even-column-color", getColorProperty("even-column-color", new Color(0,0,128)));
    setColorProperty("text-column-color", getColorProperty("text-column-color", UIManager.getColor("TextArea.foreground")));
    setColorProperty("caret-color", getColorProperty("caret-color", UIManager.getColor("TextArea.foreground")));
  }

  private static String findAppDir() {
    String hexpoundDir = null;
    String classPath = System.getProperty("java.class.path");
    int index = classPath.toLowerCase().indexOf("hexpound.jar");
    int start = classPath.lastIndexOf(File.pathSeparator, index) + 1;
    if(classPath.equalsIgnoreCase("hexpound.jar")) {
      hexpoundDir = System.getProperty("user.dir");
    } else if(index > start) {
      hexpoundDir = classPath.substring(start, index - 1);
    } else {
      hexpoundDir = System.getProperty("user.dir");
      System.out.println("hexpound.jar is not in the class path");
      System.out.println("using "+hexpoundDir+" as home directory");
    }
    return hexpoundDir;
  }

  private static String findUserDataDir() {
    String dir;
    String appData = System.getenv("APPDATA");
    if(appData != null) {
      dir = appData + File.separator + "Hexpound";
    } else {
      dir = System.getProperty("user.home") + File.separator + ".hexpound";
    }
    return dir;
  }

  public static void main(final String[] args) {
    //check VM version
    if(System.getProperty("java.version").compareTo("1.6") < 0) {
      System.out.println("Hexpound requires Java 1.6 or newer.");
      if(System.getProperty("java.version").compareTo("1.2") >= 0)
        JOptionPane.showMessageDialog(null, "Hexpound requires Java 1.6 or newer to run. You have version "+System.getProperty("java.version")+"."+"\nGo to www.java.com to download the newest Java VM.", "Hexpound", JOptionPane.INFORMATION_MESSAGE);
      System.exit(1);
    }

    new File(userDataDir).mkdirs();
    new File(userDataDir + File.separator + "scripts").mkdir();

    loadProperties();
    setDefaultProperties();

    SpeedTimer timer = new SpeedTimer().start();
    loadActions();
    timer.stop();
    timer.print("Loading actions");
    /*System.out.println(actions.size());
    String[] names = getActionNames();
    for(int i=0; i<names.length; i++) {
      System.out.println(getAction(names[i]));
    }*/

    setDefaultColorProperties();

    try {
      UIManager.setLookAndFeel(getProperty("look-and-feel"));
    } catch(Exception e) {
      System.out.println("error setting look and feel");
    }
    JFrame.setDefaultLookAndFeelDecorated(getBooleanProperty("decorate-frames"));
    JDialog.setDefaultLookAndFeelDecorated(getBooleanProperty("decorate-dialogs"));

    EventQueue.invokeLater(new Runnable() {
      public void run() {
        window = new HexpoundWindow();
        propertiesChanged(); //after they are loaded

        //parse command line arguments
        for(int i=0; i<args.length; i++) {
          openFile(new File(args[i]));
        }

        window.setVisible(true);
        updateFileActionsEnabled();
        updateOtherActionsEnabled();
      }
    });
    autoUpdateTimer = new Timer(true);
    autoUpdateTimer.schedule(new TimerTask() {
      public void run() {
        if(getProperty("version-check.ver").equals(getVersion()))
          return;
        if(getProperty("version-check.day").equals(Integer.toString(new GregorianCalendar().get(Calendar.DAY_OF_YEAR))))
          return;
        EventQueue.invokeLater(new Runnable() {
          public void run() {
            checkForUpdate(false);
          }
        });
      }
    }, 30*60000, 30*60000);
  }

  public static String getVersion() {
    return "0.10";
  }

  public static String[] getActionNames() {
    String[] strs = new String[actions.size()];
    Iterator it = actions.keySet().iterator();
    for(int i=0; it.hasNext(); i++) {
      strs[i] = (String)it.next();
    }
    return strs;
  }
  public static AdvancedAction getAction(String name) {
    return (AdvancedAction)actions.get(name);
  }

  public static int getBufferCount() {
    return buffers.size();
  }
  public static Buffer getBuffer(int index) {
    return (Buffer)buffers.get(index);
  }
  public static Buffer getCurrentBuffer() {
    if(buffers.size() < 1)
      return null;
    return (Buffer)buffers.get(window.getSelectedTabIndex());
  }

  public static void propertiesChanged() {
    //try {
    //  UIManager.setLookAndFeel(getProperty("look-and-feel"));
    //} catch(Exception e) {
    //  System.out.println("error setting look and feel");
    //}
    //SwingUtilities.updateComponentTreeUI(window);
    //window.getToolBar().updateUI();
    JHexArea.propertiesChanged();
    String prop = Hexpound.getProperty("tab-placement");
    if(prop.equals("top") || prop.equals("bottom") || prop.equals("left") || prop.equals("right"))
      window.setTabPlacement(prop);
  }

  //updates whether the actions are enabled and whether they are checked
  static void updateFileActionsEnabled() {
    boolean fileOpen = getBufferCount() > 0;
    getAction("close").setEnabled(fileOpen);
    getAction("save").setEnabled(fileOpen);
    getAction("save-as").setEnabled(fileOpen);
    //getAction("undo").setEnabled(fileOpen);
    //getAction("redo").setEnabled(fileOpen);
    getAction("paste").setEnabled(fileOpen);
    getAction("select-all").setEnabled(fileOpen);
    getAction("find").setEnabled(fileOpen);
    getAction("find-next").setEnabled(fileOpen);
    getAction("go-to-offset").setEnabled(fileOpen);
    getAction("scroll-to-caret").setEnabled(fileOpen);
    getAction("go-to-previous-tab").setEnabled(fileOpen);
    getAction("go-to-next-tab").setEnabled(fileOpen);
  }
  //this should be called every time the user switches between files,
  //loads or unloads a table, and when the selection changes
  static void updateOtherActionsEnabled() {
    boolean fileOpen = getBufferCount() > 0;
    boolean hasSel = fileOpen && window.getCurrentHexArea().hasSelection();
    getAction("cut").setEnabled(hasSel);
    getAction("copy").setEnabled(hasSel);
    getAction("copy-plain-text").setEnabled(hasSel);

    getAction("load-table").setEnabled(fileOpen);
    getAction("unload-table").setEnabled(fileOpen && getCurrentBuffer().getTable()!=null);
  }

  /**
   * Parses str into a int. It will be parsed with radix 10 unless it:
   * starts with "0x", "0X", or "#", or ends with "h" - radix 16
   * starts with "0" - radix 8
   */
  public static int decodeInt(String str) throws NumberFormatException {
    str = removeWhitespace(str);
    if(str.endsWith("h"))
      str = "0x"+str.substring(0, str.length()-1);
    return Integer.decode(str).intValue();
  }

  /**
   * Removes the whitespace from the specified string. It is here for removing
   * whitespace from numbers, but should work fine with any string.
   */
  public static String removeWhitespace(String str) {
    StringBuffer strB = new StringBuffer(); //change when dropping 1.4
    for(int i = 0; i<str.length(); i++) {
      if(str.charAt(i) > '\u0020')
        strB.append(str.charAt(i));
    }
    return strB.toString();
  }

  static int hexDigitValue(char digit) {
    if(digit >= 'a' && digit <= 'f')
      return 10 + digit - 'a';
    if(digit >= 'A' && digit <= 'F')
      return 10 + digit - 'A';
    if(digit >= '0' && digit <= '9')
      return digit - '0';
    return -1;
  }

  /**
   * Converts the specified string of hexadecimal characters into a byte array.
   *
   * Any whitespace in the string is ignored.
   */
  public static byte[] parseHex(String str) throws java.text.ParseException {
    ByteArray arr = new ByteArray();
    boolean inByte = false;
    byte firstNibble = 0;
    for(int i = 0; i < str.length(); ++i) {
      if(" \t\r\n".indexOf(str.charAt(i)) != -1)
        continue;
      int val = hexDigitValue(str.charAt(i));
      if(val == -1)
        throw new java.text.ParseException("Invalid hex digit " + str.charAt(i) + " in the string.", i);
      if(inByte) {
        arr.add((byte)(firstNibble << 4 | val));
      } else {
        firstNibble = (byte)val;
      }
      inByte = !inByte;
    }
    if(inByte)
      throw new java.text.ParseException("The string must contain an even number of hex digits.", str.length());

    return arr.toArray();
  }

  //variables set:
  //window - the window
  //buffer - the current, if any
  //hexArea - the current, if any
  public static Object executeScript(final File f) {
    if(f == null)
      throw new NullPointerException("f must not be null");
    ScriptProgressDialog progressDialog = new ScriptProgressDialog(window, f.getName());
    Object ret = null;
    try {
      Interpreter bsh = new Interpreter();
      bsh.set("window", window);
      bsh.set("buffer", getCurrentBuffer());
      bsh.set("hexArea", window.getCurrentHexArea());
      bsh.set("progressDialog", progressDialog);
      bsh.set("progressBar", progressDialog.getProgressBar());
      ret = bsh.source(f.getPath());
    } catch(IOException e) {
      System.out.println("Error reading script file.");
    } catch(EvalError e) {
      new ExceptionDialog(window, "Script exception:", "Error", e.toString()).setVisible(true);
    }
    progressDialog.setVisible(false);
    return ret;
  }
  public static void showEvaluateDialog() {
    String str = JOptionPane.showInputDialog(window, "Enter expression:\n\n" + "Variables:\n"
    + "  window - the main window instance\n"
    + "  buffer - the current buffer, if any\n"
    + "  hexArea - the current hex area, if any");
    if(str == null)
      return;
    try {
      Interpreter bsh = new Interpreter();
      bsh.set("window", window);
      bsh.set("buffer", getCurrentBuffer());
      bsh.set("hexArea", window.getCurrentHexArea());
      Object result = bsh.eval(str);
      JOptionPane.showMessageDialog(window, String.valueOf(result), "Result", JOptionPane.INFORMATION_MESSAGE);
    } catch(EvalError e) {
      new ExceptionDialog(window, "Exception:", "Error", e.toString()).setVisible(true);
    }
  }

  public static File[] getScriptFiles() {
    String[] paths = {
      userDataDir + File.separator + "scripts",
      appDir + File.separator + "scripts"
    };

    ArrayList<File> files = new ArrayList<File>();
    for(int i = 0; i < paths.length; ++i) {
      final File[] tmpFiles = new File(paths[i]).listFiles(new java.io.FileFilter() {
        public boolean accept(File pathname) {
          return pathname.getName().endsWith(".bsh");
        }
      });
      if(tmpFiles != null) {
        files.addAll(Arrays.asList(tmpFiles));
      }
    }

    File[] filesArr = new File[files.size()];
    files.toArray(filesArr);
    return filesArr;
  }

  public static void newFile() {
    Buffer buffer = new Buffer();
    buffers.add(buffer);
    window.addTab(buffer);
    updateFileActionsEnabled();
  }

  public static void openFile() {
    JFileChooser fc = new JFileChooser();
    fc.setCurrentDirectory(new File(getProperty("last-file-dir", "")));
    if(fc.showOpenDialog(window) != JFileChooser.APPROVE_OPTION)
      return;
    setProperty("last-file-dir", fc.getCurrentDirectory().getAbsolutePath());
    File selFile = fc.getSelectedFile();
    if(!selFile.exists()) {
      JOptionPane.showMessageDialog(window, "The file you selected does not exist.", "Hexpound", JOptionPane.ERROR_MESSAGE);
      return;
    }
    openFile(selFile);
  }
  public static void openFile(File file) {
    Buffer buffer = new Buffer(file);
    buffers.add(buffer);
    window.addTab(buffer);
    updateFileActionsEnabled();
  }

  //returns true if the file was actually saved
  public static boolean saveFile() {
    return saveFile(window.getSelectedTabIndex());
  }
  //returns true if the file was actually saved
  public static boolean saveFile(int index) {
    if(getBuffer(index).getFile() == null) {
      return saveFileAs(index);
    }
    try {
      OutputStream out = new BufferedOutputStream(new FileOutputStream(getBuffer(index).getFile()));
      for(int i=0; i<getBuffer(index).getLength(); i++) {
        out.write(getBuffer(index).getByteAt(i));
      }
      out.close();
      getBuffer(index).setDirty(false);
      return true;
    } catch(IOException ex) {
      JOptionPane.showMessageDialog(getWindow(), "An error occurred writing the file.\n"+ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
      return false;
    }
  }

  //returns true if the file was actually saved, false otherwise
  public static boolean saveFileAs() {
    return saveFileAs(window.getSelectedTabIndex());
  }
  public static boolean saveFileAs(int index) {
    JFileChooser fc = new JFileChooser();
    fc.setCurrentDirectory(new File(getProperty("last-file-dir", "")));
    if(fc.showSaveDialog(window) != JFileChooser.APPROVE_OPTION)
      return false;
    setProperty("last-file-dir", fc.getCurrentDirectory().getAbsolutePath());
    if(fc.getSelectedFile().exists()) {
      if(JOptionPane.showConfirmDialog(window, "The file you selected already exists.\nAre you sure you want to overwrite it?", "Hexpound", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE) == JOptionPane.NO_OPTION)
        return false;
    }

    getBuffer(index).setFile(fc.getSelectedFile());
    window.getTabbedPane().setTitleAt(window.getSelectedTabIndex(), getBuffer(index).getFile().getName());
    window.getTabbedPane().setToolTipTextAt(window.getSelectedTabIndex(), fc.getSelectedFile().getAbsolutePath());
    return saveFile(index);
  }

  //return true if it was closed, false otherwise
  public static boolean closeFile() {
    return closeFile(window.getSelectedTabIndex());
  }
  //return true if it was closed, false otherwise
  public static boolean closeFile(int index) {
    if(getCurrentBuffer() == null)
      return false;
    if(getBuffer(index).isDirty()) {
      int option = JOptionPane.showConfirmDialog(getWindow(), "Save changes to "+getBuffer(index).getName()+"?", "Hexpound", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE);
      if(option == JOptionPane.CANCEL_OPTION)
        return false;
      else if(option == JOptionPane.YES_OPTION)
        if(!saveFile(index)) //if the file wasn't saved
          return false;
    }
    _closeFile(index);
    return true;
  }
  public static void _closeFile(int index) {
    if(getCurrentBuffer() == null)
      return;
    buffers.remove(index);
    window.removeTab(index);
    updateFileActionsEnabled();
  }
  public static void exit() {
    while(buffers.size() > 0) {
      if(getBuffer(0).isDirty()) {
        int option = JOptionPane.showConfirmDialog(getWindow(), "Save changes to "+getBuffer(0).getName()+"?", "Hexpound", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE);
        if(option == JOptionPane.CANCEL_OPTION)
          return;
        if(option == JOptionPane.YES_OPTION)
          if(!saveFile(0)) //if user canceled file chooser
            continue;
      }
      _closeFile(0);
    }
    saveProperties();
    System.exit(0);
  }
  public static void loadTable() {
    if(getCurrentBuffer() == null)
      return;
    JFileChooser fc = new JFileChooser();
    fc.setDialogTitle("Open Table");
    fc.addChoosableFileFilter(new javax.swing.filechooser.FileFilter() {
      public boolean accept(File f) {
        return f.isDirectory() || f.getName().endsWith(".tbl");
      }
      public String getDescription() {
        return "Table File (*.tbl)";
      }
    });
    fc.setCurrentDirectory(new File(getProperty("last-table-dir", "")));
    if(fc.showOpenDialog(getWindow()) != JFileChooser.APPROVE_OPTION)
      return;
    setProperty("last-table-dir", fc.getCurrentDirectory().getAbsolutePath());
    File selFile = fc.getSelectedFile();
    if(!selFile.exists()) {
      JOptionPane.showMessageDialog(getWindow(), "The file you selected does not exist.", "Hexpound", JOptionPane.ERROR_MESSAGE);
      return;
    }
    loadTable(selFile);
    updateOtherActionsEnabled();
  }
  public static void loadTable(File file) {
    Table t = null;
    try {
      t = new Table(file);
    } catch(IOException ex) {
      JOptionPane.showMessageDialog(getWindow(), "Error opening table file.", "Error", JOptionPane.ERROR_MESSAGE);
    } catch(Table.ParseException ex) {
      JOptionPane.showMessageDialog(getWindow(), ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
    }
    getCurrentBuffer().setTable(t);
  }
  public static void unloadTable() {
    if(getCurrentBuffer() == null)
      return;
    getCurrentBuffer().setTable(null);
    updateOtherActionsEnabled();
  }

  public static void rescanScripts() {
    window.updateScriptsMenu(getScriptFiles());
  }

  //checks if there is a newer version out yet
  //if there is, notify the user
  //if showMessages is true the user will be told if there is an error or if they have the newest version
  //should only download the update file once per day
  public static void checkForUpdate(boolean showMessages) {
    System.out.println("checkForUpdate()");
    String address = "http://members.aol.com/truescienc/hexpound/update.txt";
    try {
      BufferedReader reader = new BufferedReader(new InputStreamReader(new URL(address).openConnection().getInputStream()));
      String stableVer = reader.readLine();
      setProperty("version-check.day", Integer.toString(new GregorianCalendar().get(Calendar.DAY_OF_YEAR)));
      if(stableVer == null)
        return;
      if(stableVer.equals(getVersion())) {
        if(showMessages) {
          JOptionPane.showMessageDialog(window, "You are using the newest version of Hexpound.", "Hexpound", JOptionPane.INFORMATION_MESSAGE);
          return;
        }
      }
      JOptionPane.showMessageDialog(window, "There is a new version of Hexpound out: "+stableVer+"\nGo to http://members.aol.com/truescienc/hexpound/ to download it.");
      setProperty("version-check.ver", getVersion());
    } catch(IOException e) {
      if(showMessages)
        JOptionPane.showMessageDialog(window, "The update file could not be loaded.", "Hexpound", JOptionPane.ERROR_MESSAGE);
    }
  }
}
