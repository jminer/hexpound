/*
 * ExceptionDialog.java - Shows an exception message and stack trace
 * Copyright (C) 2004, 2005 Jordan Miner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package hexpound;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Frame;
import javax.swing.border.EmptyBorder;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class ExceptionDialog extends EnhancedDialog {
  protected JTextArea textArea = new JTextArea();
  protected JButton okButton = new JButton("OK");

  public ExceptionDialog(Frame parent, String message, String title, String text) {
    super(parent, title, true);
    textArea.setText(text);
    textArea.setEditable(false);
    textArea.setRows(10);
    textArea.setColumns(50);
    textArea.setLineWrap(true);
    okButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        ok();
      }
    });

    JPanel pane = new JPanel();
    pane.setBorder(new EmptyBorder(5,5,5,5));
    pane.setLayout(new BoxLayout(pane, BoxLayout.Y_AXIS));
    JLabel label = new JLabel(message);
    label.setAlignmentX(Component.CENTER_ALIGNMENT);
    pane.add(label);
    pane.add(new JScrollPane(textArea));
    okButton.setAlignmentX(Component.CENTER_ALIGNMENT);
    pane.add(okButton);

    getContentPane().add(pane);
    pack();
    setLocationRelativeTo(parent);
  }
  public void ok() {
    setVisible(false);
  }
  public void cancel() {
    setVisible(false);
  }
}
