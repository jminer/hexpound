/*
 * ScriptProgressDialog.java - A dialog to be used by scripts to show progress
 * Copyright (C) 2004, 2005 Jordan Miner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package hexpound;

import java.awt.Dimension;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;

public class ScriptProgressDialog extends EnhancedDialog {
  private JProgressBar progressBar = new JProgressBar();
  private JLabel statusLabel = new JLabel(" ");

  public ScriptProgressDialog(JFrame owner, String scriptName) {
    super(owner, "Script Progress", true);
    setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);

    JPanel pane = new JPanel();
    pane.setLayout(new BoxLayout(pane, BoxLayout.Y_AXIS));
    pane.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));

    pane.add(new JLabel("Running script: "+scriptName));
    pane.add(Box.createVerticalStrut(5));
    progressBar.setPreferredSize(new Dimension(200, progressBar.getPreferredSize().height));
    pane.add(progressBar);
    pane.add(Box.createVerticalStrut(5));
    pane.add(statusLabel);

    getContentPane().add(pane);
    pack();
    setLocationRelativeTo(owner);
  }
  public JProgressBar getProgressBar() {
    return progressBar;
  }
  public void setStatusString(String str) {
    statusLabel.setText(str);
  }

  public void ok() {}
  public void cancel() {}
}
