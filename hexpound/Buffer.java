/*
 * Buffer.java - Holds the data from an open file
 * Copyright (C) 2004, 2005 Jordan Miner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package hexpound;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import javax.swing.event.EventListenerList;

//TODO:** fireBufferChanged() when it is
public class Buffer {
  private byte[] value;
  private int length;
  private boolean newFile;
  private boolean dirty;
  private boolean closed;
  private File file;
  private Table table;

  //opens new unsaved file
  Buffer() {
    newFile = true;
    dirty = false;
    value = new byte[1024];
  }
  //opens an already existant file
  Buffer(File file) {
    if(file==null)
      throw new NullPointerException("file must be non-null");
    this.file = file;
    newFile = false;
    dirty = false;
    FileInputStream fis = null;
    try {
      fis = new FileInputStream(file);
      value = new byte[(int)file.length()];
      fis.read(value);
      length = value.length;
    } catch(IOException e) {
    }
  }
/*  protected void finalize() throws Throwable {
    super.finalize();
    System.out.println(getName()+"  Buffer.finalize(), but this isn't being called :(");
  }*/

  //public boolean isNewFile() {
  //  return newFile;
  //}
  public boolean isDirty() {
    return dirty;
  }
  public void setDirty(boolean b) {
    dirty = b;
  }
  public String getName() {
    if(file == null)
      return "untitled";
    else
      return file.getName();
  }
  public File getFile() {
    return file;
  }
  public void setFile(File newFile) {
    file = newFile;
  }

  public Table getTable() {
    return table;
  }
  public void setTable(Table t) {
    table = t;
    StatusBar.update(); //TODO: probably ought to change
    fireBufferChanged();
  }

  public void ensureCapacity(int minimum) {
    if(minimum < 0 || minimum <= length)
      return;
    byte[] newValue = new byte[Math.max(minimum, length+1024)]; //only grow 1KB
    System.arraycopy(value, 0, newValue, 0, length);
    value = newValue;
  }
  public int getLength() {
    return length;
  }

  public byte getByteAt(int index) {
    if(index < 0 || index >= length)
      throw new IndexOutOfBoundsException("index = "+index);
    return value[index];
  }

  public void setByteAt(int index, byte b) {
    dirty = true;
    if(index < 0 || index >= length)
      throw new IndexOutOfBoundsException("index = "+index);
    value[index] = b;
    fireBufferChanged();
  }

  public byte[] getBytes(int start, int end) {
    if(start < 0 || start > length || start > end)
      throw new IndexOutOfBoundsException("start = "+start);
    if(end > length)
      end = length;
    byte[] returnValue = new byte[end-start];
    for(int i=start; i<end; i++) {
      returnValue[i-start] = value[i];
    }
    return returnValue;
  }

  public short getShortAt(int index) {
    if(index < 0 || index+1 >= length)
      throw new IndexOutOfBoundsException("index = "+index);
    return (short)(value[index] << 8 | value[index+1]);
  }

  public void setShortAt(int index, short s) {
    dirty = true;
    if(index < 0 || index+1 >= length)
      throw new IndexOutOfBoundsException("index = "+index);
    value[index] = (byte)(s >> 8);
    value[index+1] = (byte)s;
    fireBufferChanged();
  }

  public void setCharAt(int index, char c) {
    dirty = true;
    if(index < 0 || index >= length)
      throw new IndexOutOfBoundsException("index = "+index);
    if(table == null)
      value[index] = (byte)c;
    else
      value[index] = table.convert(c);
    fireBufferChanged();
  }

  //{{{ Text getters
  /**
   * Converts the byte at the specified index of this buffer to text with the
   * current table and returns the text. If the table is null, or the byte is
   * not in the table, the byte will be converted using the default encoding.
   *
   * @param index            the index of the byte to convert
   * @param onlyCharsInTable if true, any char that the table cannot convert
   *                         will be returned as the unknown char, specified
   *                         by the Hexpound property "unknown-char"
   */
  public String getText(int index) {
    if(index < 0 || index >= length)
      throw new IndexOutOfBoundsException("index = "+index);
    Table tbl = table;
    if(tbl == null)
      tbl = new Table();

    return tbl.convert(value[index]);
  }
  /**
   * Converts the bytes from start to end of this buffer to text with the
   * current table and returns the text. If the table is null, or the byte is
   * not in the table, the byte will be converted using the default encoding.
   *
   * @param start            the beginning offset, inclusive
   * @param end              the ending offset, exclusive
   * @param onlyCharsInTable if true, any char that the table cannot convert
   *                         will be returned as the unknown char, specified
   *                         by the Hexpound property "unknown-char"
   */
  public String getText(int start, int end) {
    if(start < 0 || start > length || start > end)
      throw new IndexOutOfBoundsException("start = "+start);
    if(end > length)
      end = length;
    Table tbl = table;
    if(tbl == null)
      tbl = new Table();

    StringBuffer str = new StringBuffer();
    for(int i=start; i<end; i++) {
      str.append(tbl.convert(value[i]));
    }
    return str.toString();
  }
  //}}}

  /**
   * Inserts the specified byte at offset.
   *
   * @param offset the offset
   * @param b      the byte to insert
   * @exception IndexOutOfBoundsException if offset is negative or
   *            more than getLength()
   */
  public void insert(int offset, byte b) {
    dirty = true;
    if(offset < 0 || offset > length)
      throw new IndexOutOfBoundsException("offset = "+offset);
    ensureCapacity(length+1);
    System.arraycopy(value, offset, value, offset+1, length-offset);
    value[offset] = b;
    length++;
    fireBufferChanged();
  }

  public void insert(int offset, byte[] b) {
    dirty = true;
    if(offset < 0 || offset > length)
      throw new IndexOutOfBoundsException("offset = "+offset);
    ensureCapacity(length+b.length);
    System.arraycopy(value, offset, value, offset+b.length, length-offset);
    System.arraycopy(b, 0, value, offset, b.length);
    length += b.length;
    fireBufferChanged();
  }

  /**
   * Inserts the specified char at offset.
   *
   * @param offset the offset
   * @param c      the char to insert
   * @exception IndexOutOfBoundsException if offset is negative or
   *            more than getLength()
   */
  public void insert(int offset, char c) {
    dirty = true;
    if(offset < 0 || offset > length)
      throw new IndexOutOfBoundsException("offset = "+offset);
    Table tbl = table;
    if(tbl == null)
      tbl = new Table();

    insert(offset, tbl.convert(c));
    fireBufferChanged();
  }

  public void insert(int offset, String str) {
    dirty = true;
    if(offset < 0 || offset > length)
      throw new IndexOutOfBoundsException("offset = "+offset);
    Table tbl = table;
    if(tbl == null)
      tbl = new Table();

    byte[] tmp = new byte[str.length()];
    for(int i=0; i<str.length(); i++) {
      tmp[i] = tbl.convert( str.charAt(i) );
    }
    insert(offset, tmp);
    fireBufferChanged();
  }

  /**
   * Deletes the specified bytes.
   *
   * @param start  The beginning index, inclusive
   * @param end    The ending index, exclusive
   * @exception IndexOutOfBoundsException  if start is negative,
   *            greater than getLength(), or more than end
   */
  public void delete(int start, int end) {
    dirty = true;
    if(start < 0 || start > length || start > end)
      throw new IndexOutOfBoundsException("start = "+start);
    if(end > length)
      end = length;
    System.arraycopy(value, end, value, start, value.length-end);
    length -= end-start;
    fireBufferChanged();
  }

  //{{{ searching methods
  public int indexOf(byte[] b) {
    return indexOf(b, 0);
  }
  public int indexOf(byte[] b, int fromIndex) {
    if(fromIndex < 0 || fromIndex > length)
      throw new IndexOutOfBoundsException("fromIndex = "+fromIndex);

top:                      //subtract b.length so i+j won't be past end of file
    for(int i=fromIndex; i<value.length-b.length; i++) {
      for(int j=0; j < b.length; j++) { //go over b, checking with value
        if(value[i+j] != b[j])
          continue top;
        if(j == b.length-1)
          return i+j;
      }
    }
    return -1;
  }

  /**
   * Holds the information from a search. This is needed because the length
   * of the search string may not equal the number of matching bytes.
   */
  public class FindInfo {
    /**
     * The index of the first byte that matches search string.
     */
    int index = -1;
    /**
     * The length of bytes that match search string.
     */
    int length;
    public String toString() {
      return "FindInfo["+"index="+index+", length="+length+"]";
    }
  }

  public FindInfo indexOf(String str) {
    return indexOf(str, 0);
  }
  public FindInfo indexOf(String str, int fromIndex) {
    if(fromIndex < 0)
      fromIndex = 0;
    if(fromIndex >= length)
      fromIndex = length-1;

    Table tbl = table;
    if(tbl == null)
      tbl = new Table();

    FindInfo info = new FindInfo();
top://go over every byte
    for(int i=fromIndex; i<value.length-str.length(); i++) {
      int k=0;
      for(int j=0; j<str.length(); k++) {//j is index in String, k is times
        String tmp = tbl.convert(value[i+k]);
        if( !str.substring(j).startsWith(tmp) )
          continue top;
        j += tmp.length();
      }
      info.length = k; //k - bytes to select
      info.index = i;
      break;
    }

    return info;
  }
  public FindInfo lastIndexOf(String str) {
    return lastIndexOf(str, length-1);
  }
  public FindInfo lastIndexOf(String str, int fromIndex) {
    if(fromIndex < 0)
      fromIndex = 0;
    if(fromIndex >= length)
      fromIndex = length-1;

    Table tbl = table;
    if(tbl == null)
      tbl = new Table();

    FindInfo info = new FindInfo();
top://go over every byte  //subtract b.length so i+j won't be past end of file
    for(int i=fromIndex; i >= 0; i--) {
      int k=0;
      for(int j=0; j<str.length(); k++) {//j is index in String, k is times
        String tmp = tbl.convert(value[i-k]);
        if( !str.substring(0, str.length()-j).endsWith(tmp) )
          continue top;
        j += tmp.length();
      }
      info.length = k;//k - bytes to select
      info.index = i+1-info.length;
      break;
    }

    return info;
  }
  //}}}

  //{{{ BufferChangeListeners
  EventListenerList listenerList = new EventListenerList();

  public void addBufferChangeListener(BufferChangeListener l) {
     listenerList.add(BufferChangeListener.class, l);
  }

  public void removeBufferChangeListener(BufferChangeListener l) {
     listenerList.remove(BufferChangeListener.class, l);
  }

  protected void fireBufferChanged() {
     // Guaranteed to return a non-null array
     Object[] listeners = listenerList.getListenerList();
     // Process the listeners last to first, notifying
     // those that are interested in this event
     for (int i = listeners.length-2; i>=0; i-=2) {
         if (listeners[i]==BufferChangeListener.class) {
             ((BufferChangeListener)listeners[i+1]).bufferChanged(this);
         }
     }
  }
  //}}}
}
