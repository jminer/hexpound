/*
 * JColorButton.java - A button that shows a color chooser if clicked
 * Copyright (C) 2004, 2005 Jordan Miner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package hexpound;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import javax.swing.JButton;
import javax.swing.JColorChooser;

public class JColorButton extends JButton {
  private Color color;

  public JColorButton() {
    this(Color.BLACK);
  }

  public JColorButton(Color color) {
    super("               ");
    setColor(color);
    addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        Color c = JColorChooser.showDialog(getParent(), "Choose Color", getColor());
        if(c == null)
          return;
        setColor(c);
      }
    });
  }

  public Color getColor() {
    return color;
  }

  public void setColor(Color newColor) {
    if(newColor == null)
      throw new IllegalArgumentException("newColor must not be null");
    color = newColor;
  }
  protected void paintComponent(Graphics g) {
    super.paintComponent(g);
    Graphics2D g2d = (Graphics2D)g.create();
    g2d.setPaint(color);
    g2d.fill(new Rectangle(5, 5, getWidth()-10, getHeight()-10));
  }
}
