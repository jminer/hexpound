/*
 * OptionsDialog.java - A dialog that shows Hexpound's options
 * Copyright (C) 2004, 2005 Jordan Miner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package hexpound;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.text.NumberFormat;
import javax.swing.*;
import javax.swing.border.EmptyBorder;

//options:
//bytes displayed per line
//grouping
//font size
//unknown char
//smooth text
//offset column color
//odd column color
//even column color
//text column color
//caret color
//look and feel
  //add:
  //bold and italic text
  //need to have options to get the colors from the look and feel:
  //Offset column color: [checkbox] Use LaF color
  //Text column color: [checkbox] Use LaF color
  //Caret color: [checkbox] Use LaF color
public class OptionsDialog extends EnhancedDialog {
  //{{{ NetBeans generated varibles
  private JPanel appearanceTab;
  private JButton applyButton;
  private JCheckBox boldCheckBox;
  private JPanel byteLayoutPanel;
  private JTextField bytesPerLineTextField;
  private JButton cancelButton;
  private JPanel colorsPanel;
  private JCheckBox decorateDialogsCheckBox;
  private JCheckBox decorateFramesCheckBox;
  private JPanel fontPanel;
  private JTextField fontSizeTextField;
  private JTextField groupingTextField;
  private JPanel hexAreaTab;
  private JCheckBox italicCheckBox;
  private JPanel jPanel;
  private JComboBox lafComboBox;
  private JButton okButton;
  private JTabbedPane optionsTabPane;
  private JCheckBox showOnlyTableCharsCheckBox;
  private JCheckBox smoothTextCheckBox;
  private JPanel spacingPanel;
  private JLabel styleLabel;
  private JComboBox tabPlacementComboBox;
  private JColorButton textColumnColorButton;
  private JColorButton oddColumnColorButton;
  private JColorButton offsetColumnColorButton;
  private JColorButton evenColumnColorButton;
  private JColorButton caretColorButton;
  private JTextField unknownCharTextField;
  //}}}
  //private NumberFormat nf = NumberFormat.getIntegerInstance();
  /*private JTextField bytesPerLineTextField = new JTextField();
  private JTextField groupingTextField = new JTextField();
  private JTextField fontSizeTextField = new JTextField();
  private JTextField unknownCharTextField = new JTextField();
  private JCheckBox showOnlyTableCharsCheckBox = new JCheckBox("Show only chars from table");
  private JCheckBox smoothTextCheckBox = new JCheckBox("Use smooth text");
  private JColorButton offsetColumnColorButton = new JColorButton();
  private JColorButton oddColumnColorButton = new JColorButton();
  private JColorButton evenColumnColorButton = new JColorButton();
  private JColorButton textColumnColorButton = new JColorButton();
  private JColorButton caretColorButton = new JColorButton();
  private JComboBox lafComboBox = new JComboBox();
  private JButton okButton = new JButton("OK");
  private JButton cancelButton = new JButton("Cancel");*/

  private boolean okPressed = false;

  public OptionsDialog() {
    super(Hexpound.getWindow(), "Options", true);
    JPanel pane = new JPanel();
    pane.setBorder(new EmptyBorder(5,5,5,5));
    setContentPane(pane);
    //{{{ NetBeans generated code
    JLabel byteGroupingLabel;
    JLabel bytesPerLineLabel;
    JLabel caretLabel;
    JLabel evenColumnLabel;
    JLabel fontSizeLabel;
    GridBagConstraints gridBagConstraints;
    JLabel lafLabel;
    JLabel oddColumnLabel;
    JLabel offsetColumnLabel;
    JLabel tabPlacementLabel;
    JLabel textColumnLabel;
    JLabel unknownCharLabel;

    okButton = new JButton();
    cancelButton = new JButton();
    spacingPanel = new JPanel();
    optionsTabPane = new JTabbedPane();
    hexAreaTab = new JPanel();
    fontPanel = new JPanel();
    fontSizeLabel = new JLabel();
    fontSizeTextField = new JTextField();
    boldCheckBox = new JCheckBox();
    italicCheckBox = new JCheckBox();
    styleLabel = new JLabel();
    byteLayoutPanel = new JPanel();
    bytesPerLineLabel = new JLabel();
    bytesPerLineTextField = new JTextField();
    byteGroupingLabel = new JLabel();
    groupingTextField = new JTextField();
    colorsPanel = new JPanel();
    offsetColumnLabel = new JLabel();
    offsetColumnColorButton = new JColorButton();
    oddColumnLabel = new JLabel();
    oddColumnColorButton = new JColorButton();
    evenColumnLabel = new JLabel();
    evenColumnColorButton = new JColorButton();
    textColumnLabel = new JLabel();
    textColumnColorButton = new JColorButton();
    caretLabel = new JLabel();
    caretColorButton = new JColorButton();
    jPanel = new JPanel();
    smoothTextCheckBox = new JCheckBox();
    unknownCharLabel = new JLabel();
    unknownCharTextField = new JTextField();
    showOnlyTableCharsCheckBox = new JCheckBox();
    appearanceTab = new JPanel();
    lafLabel = new JLabel();
    lafComboBox = new JComboBox();
    decorateFramesCheckBox = new JCheckBox();
    decorateDialogsCheckBox = new JCheckBox();
    tabPlacementLabel = new JLabel();
    tabPlacementComboBox = new JComboBox();
    applyButton = new JButton();

    getContentPane().setLayout(new java.awt.GridBagLayout());

    okButton.setText("OK");
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 1;
    gridBagConstraints.gridy = 1;
    gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
    gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
    getContentPane().add(okButton, gridBagConstraints);

    cancelButton.setText("Cancel");
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 2;
    gridBagConstraints.gridy = 1;
    gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
    gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
    getContentPane().add(cancelButton, gridBagConstraints);

    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 0;
    gridBagConstraints.gridy = 1;
    gridBagConstraints.weightx = 0.5;
    getContentPane().add(spacingPanel, gridBagConstraints);

    hexAreaTab.setLayout(new java.awt.GridBagLayout());

    fontPanel.setLayout(new java.awt.GridBagLayout());

    fontPanel.setBorder(new javax.swing.border.TitledBorder("Font"));
    fontSizeLabel.setText("Size:");
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 0;
    gridBagConstraints.gridy = 0;
    gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
    gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
    fontPanel.add(fontSizeLabel, gridBagConstraints);

    fontSizeTextField.setColumns(5);
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 1;
    gridBagConstraints.gridy = 0;
    gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
    gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
    gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
    fontPanel.add(fontSizeTextField, gridBagConstraints);

    boldCheckBox.setText("Bold");
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 3;
    gridBagConstraints.gridy = 0;
    gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
    gridBagConstraints.insets = new java.awt.Insets(0, 3, 0, 3);
    fontPanel.add(boldCheckBox, gridBagConstraints);

    italicCheckBox.setText("Italic");
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 3;
    gridBagConstraints.gridy = 1;
    gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
    gridBagConstraints.insets = new java.awt.Insets(0, 3, 0, 3);
    fontPanel.add(italicCheckBox, gridBagConstraints);

    styleLabel.setText("Style:");
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 2;
    gridBagConstraints.gridy = 0;
    gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
    fontPanel.add(styleLabel, gridBagConstraints);

    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
    gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
    hexAreaTab.add(fontPanel, gridBagConstraints);

    byteLayoutPanel.setLayout(new java.awt.GridBagLayout());

    byteLayoutPanel.setBorder(new javax.swing.border.TitledBorder("Byte Layout"));
    bytesPerLineLabel.setText("Bytes per line:");
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 0;
    gridBagConstraints.gridy = 0;
    gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
    gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
    byteLayoutPanel.add(bytesPerLineLabel, gridBagConstraints);

    bytesPerLineTextField.setColumns(5);
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 1;
    gridBagConstraints.gridy = 0;
    gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
    gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
    gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
    byteLayoutPanel.add(bytesPerLineTextField, gridBagConstraints);

    byteGroupingLabel.setText("Bytes in a group:");
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 0;
    gridBagConstraints.gridy = 1;
    gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
    gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
    byteLayoutPanel.add(byteGroupingLabel, gridBagConstraints);

    groupingTextField.setColumns(5);
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 1;
    gridBagConstraints.gridy = 1;
    gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
    gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
    gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
    byteLayoutPanel.add(groupingTextField, gridBagConstraints);

    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 1;
    gridBagConstraints.gridy = 0;
    gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
    gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
    hexAreaTab.add(byteLayoutPanel, gridBagConstraints);

    colorsPanel.setLayout(new java.awt.GridBagLayout());

    colorsPanel.setBorder(new javax.swing.border.TitledBorder("Colors"));
    offsetColumnLabel.setText("Offset column:");
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
    gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
    colorsPanel.add(offsetColumnLabel, gridBagConstraints);

    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 1;
    gridBagConstraints.gridy = 0;
    gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
    gridBagConstraints.weightx = 0.5;
    gridBagConstraints.insets = new java.awt.Insets(1, 3, 3, 3);
    colorsPanel.add(offsetColumnColorButton, gridBagConstraints);

    oddColumnLabel.setText("Odd column:");
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 2;
    gridBagConstraints.gridy = 0;
    gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
    gridBagConstraints.insets = new java.awt.Insets(3, 7, 3, 3);
    colorsPanel.add(oddColumnLabel, gridBagConstraints);

    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 3;
    gridBagConstraints.gridy = 0;
    gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
    gridBagConstraints.weightx = 0.5;
    gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
    colorsPanel.add(oddColumnColorButton, gridBagConstraints);

    evenColumnLabel.setText("Even column:");
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 2;
    gridBagConstraints.gridy = 1;
    gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
    gridBagConstraints.insets = new java.awt.Insets(3, 7, 3, 3);
    colorsPanel.add(evenColumnLabel, gridBagConstraints);

    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 3;
    gridBagConstraints.gridy = 1;
    gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
    gridBagConstraints.weightx = 0.5;
    gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
    colorsPanel.add(evenColumnColorButton, gridBagConstraints);

    textColumnLabel.setText("Text column:");
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 0;
    gridBagConstraints.gridy = 1;
    gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
    gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
    colorsPanel.add(textColumnLabel, gridBagConstraints);

    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 1;
    gridBagConstraints.gridy = 1;
    gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
    gridBagConstraints.weightx = 0.5;
    gridBagConstraints.insets = new java.awt.Insets(1, 3, 3, 3);
    colorsPanel.add(textColumnColorButton, gridBagConstraints);

    caretLabel.setText("Caret:");
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 0;
    gridBagConstraints.gridy = 2;
    gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
    gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
    colorsPanel.add(caretLabel, gridBagConstraints);

    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 1;
    gridBagConstraints.gridy = 2;
    gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
    gridBagConstraints.weightx = 0.5;
    gridBagConstraints.insets = new java.awt.Insets(1, 3, 3, 3);
    colorsPanel.add(caretColorButton, gridBagConstraints);

    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 0;
    gridBagConstraints.gridy = 1;
    gridBagConstraints.gridwidth = 2;
    gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
    gridBagConstraints.weighty = 0.1;
    gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
    hexAreaTab.add(colorsPanel, gridBagConstraints);

    jPanel.setLayout(new java.awt.GridBagLayout());

    smoothTextCheckBox.setText("Smooth text");
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 0;
    gridBagConstraints.gridy = 2;
    gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
    gridBagConstraints.insets = new java.awt.Insets(0, 3, 0, 3);
    jPanel.add(smoothTextCheckBox, gridBagConstraints);

    unknownCharLabel.setText("Unknown character:");
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 0;
    gridBagConstraints.gridy = 0;
    gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
    gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
    jPanel.add(unknownCharLabel, gridBagConstraints);

    unknownCharTextField.setColumns(5);
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 1;
    gridBagConstraints.gridy = 0;
    gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
    gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
    jPanel.add(unknownCharTextField, gridBagConstraints);

    showOnlyTableCharsCheckBox.setText("Show only chars from table");
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 0;
    gridBagConstraints.gridy = 1;
    gridBagConstraints.gridwidth = 2;
    gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
    gridBagConstraints.insets = new java.awt.Insets(0, 3, 0, 3);
    jPanel.add(showOnlyTableCharsCheckBox, gridBagConstraints);

    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 0;
    gridBagConstraints.gridy = 2;
    gridBagConstraints.gridwidth = 2;
    gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
    hexAreaTab.add(jPanel, gridBagConstraints);

    optionsTabPane.addTab("Hex Area", hexAreaTab);

    appearanceTab.setLayout(new java.awt.GridBagLayout());

    lafLabel.setText("Look and feel:");
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
    gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
    appearanceTab.add(lafLabel, gridBagConstraints);

    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
    gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
    gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
    appearanceTab.add(lafComboBox, gridBagConstraints);

    decorateFramesCheckBox.setText("Draw window borders with look and feel");
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 0;
    gridBagConstraints.gridy = 1;
    gridBagConstraints.gridwidth = 2;
    gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
    gridBagConstraints.insets = new java.awt.Insets(0, 3, 0, 3);
    appearanceTab.add(decorateFramesCheckBox, gridBagConstraints);

    decorateDialogsCheckBox.setText("Draw dialog borders with look and feel");
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 0;
    gridBagConstraints.gridy = 2;
    gridBagConstraints.gridwidth = 2;
    gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
    gridBagConstraints.insets = new java.awt.Insets(0, 3, 0, 3);
    appearanceTab.add(decorateDialogsCheckBox, gridBagConstraints);

    tabPlacementLabel.setText("Tab placement:");
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 0;
    gridBagConstraints.gridy = 3;
    gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
    gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
    appearanceTab.add(tabPlacementLabel, gridBagConstraints);

    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 1;
    gridBagConstraints.gridy = 3;
    gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
    gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
    gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
    appearanceTab.add(tabPlacementComboBox, gridBagConstraints);

    optionsTabPane.addTab("Appearance", appearanceTab);

    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridwidth = 4;
    gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
    getContentPane().add(optionsTabPane, gridBagConstraints);

    applyButton.setText("Apply");
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 3;
    gridBagConstraints.gridy = 1;
    gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
    gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
    getContentPane().add(applyButton, gridBagConstraints);
    //}}}

    //{{{ configure components
    fontSizeTextField.setText(Hexpound.getProperty("font-size"));
    boldCheckBox.setSelected((Hexpound.getIntegerProperty("font-style") & Font.BOLD) != 0);
    italicCheckBox.setSelected((Hexpound.getIntegerProperty("font-style") & Font.ITALIC) != 0);
    bytesPerLineTextField.setText(Hexpound.getProperty("bytes-per-line"));
    groupingTextField.setText(Hexpound.getProperty("grouping"));
    unknownCharTextField.setText(Hexpound.getProperty("unknown-char"));
    showOnlyTableCharsCheckBox.setSelected(Hexpound.getBooleanProperty("show-only-table-chars"));
    smoothTextCheckBox.setSelected(Hexpound.getBooleanProperty("smooth-text"));
    /*backgroundImageTextField.setText(Hexpound.getProperty("background-image"));
    changeBGImageButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        JFileChooser fc = new JFileChooser();
        fc.setDialogTitle("Open Image");
        fc.addChoosableFileFilter(new javax.swing.filechooser.FileFilter() {
          public boolean accept(File f) {
            return f.isDirectory() || f.getName().endsWith(".png") || f.getName().endsWith(".jpg") || f.getName().endsWith(".gif");
          }
          public String getDescription() {
            return "Image files (*.png, *.jpg, *.gif)";
          }
        });
        if(fc.showOpenDialog(getWindow()) != JFileChooser.APPROVE_OPTION)
          return;
        backgroundImageTextField.setText(fc.getSelectedFile().getPath());
      }
    });*/

    offsetColumnColorButton.setColor(Hexpound.getColorProperty("offset-column-color"));
    textColumnColorButton.setColor(Hexpound.getColorProperty("text-column-color"));
    caretColorButton.setColor(Hexpound.getColorProperty("caret-color"));
    oddColumnColorButton.setColor(Hexpound.getColorProperty("odd-column-color"));
    evenColumnColorButton.setColor(Hexpound.getColorProperty("even-column-color"));

    UIManager.LookAndFeelInfo[] lafs = UIManager.getInstalledLookAndFeels();
    for(int i=0; i<lafs.length; i++) {
      lafComboBox.addItem(lafs[i].getName());
      //if this is current look and feel
      //if(lafs[i].getClassName() == UIManager.getLookAndFeel().getClass().getName())
      if(lafs[i].getClassName().equals(Hexpound.getProperty("look-and-feel")))
        lafComboBox.setSelectedIndex(i);
    }
    decorateFramesCheckBox.setSelected(Hexpound.getBooleanProperty("decorate-frames"));
    decorateDialogsCheckBox.setSelected(Hexpound.getBooleanProperty("decorate-dialogs"));
    tabPlacementComboBox.addItem("top");
    tabPlacementComboBox.addItem("bottom");
    tabPlacementComboBox.addItem("left");
    tabPlacementComboBox.addItem("right");
    tabPlacementComboBox.setSelectedItem(Hexpound.getProperty("tab-placement").intern());
    /*if(Hexpound.getProperty("tab-placement").equals("top"))
      tabPlacementComboBox.setSelectedIndex(0);
    else if(Hexpound.getProperty("tab-placement").equals("bottom"))
      tabPlacementComboBox.setSelectedIndex(1);
    else if(Hexpound.getProperty("tab-placement").equals("left"))
      tabPlacementComboBox.setSelectedIndex(2);
    else if(Hexpound.getProperty("tab-placement").equals("right"))
      tabPlacementComboBox.setSelectedIndex(3);*/

    okButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        ok();
      }
    });
    cancelButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        cancel();
      }
    });
    //}}}

    /*JPanel pane = new JPanel(new GridLayout(13, 2, 5, 5));
    pane.setBorder(new EmptyBorder(5,5,5,5));
    setContentPane(pane);

    pane.add(new JLabel("Bytes displayed per line:"));
    bytesPerLineTextField.setText(Hexpound.getProperty("bytes-per-line"));
    pane.add(bytesPerLineTextField);

    pane.add(new JLabel("Byte grouping:"));
    groupingTextField.setText(Hexpound.getProperty("grouping"));
    pane.add(groupingTextField);

    pane.add(new JLabel("Font size:"));
    fontSizeTextField.setText(Hexpound.getProperty("font-size"));
    pane.add(fontSizeTextField);

    pane.add(new JLabel("Unknown char:"));
    unknownCharTextField.setText(Hexpound.getProperty("unknown-char"));
    pane.add(unknownCharTextField);

    onlyShowTableCharsCheckBox.setSelected(Hexpound.getBooleanProperty("show-only-table-chars"));
    pane.add(onlyShowTableCharsCheckBox);
    pane.add(new JPanel());

    smoothTextCheckBox.setSelected(Hexpound.getBooleanProperty("smooth-text"));
    pane.add(smoothTextCheckBox);
    pane.add(new JPanel());

    pane.add(new JLabel("Offset column color:"));
    offsetColumnColorButton.setColor(Hexpound.getColorProperty("offset-column-color"));
    pane.add(offsetColumnColorButton);

    pane.add(new JLabel("Odd column color:"));
    oddColumnColorButton.setColor(Hexpound.getColorProperty("odd-column-color"));
    pane.add(oddColumnColorButton);

    pane.add(new JLabel("Even column color:"));
    evenColumnColorButton.setColor(Hexpound.getColorProperty("even-column-color"));
    pane.add(evenColumnColorButton);

    pane.add(new JLabel("Text column color:"));
    textColumnColorButton.setColor(Hexpound.getColorProperty("text-column-color"));
    pane.add(textColumnColorButton);

    pane.add(new JLabel("Caret color:"));
    caretColorButton.setColor(Hexpound.getColorProperty("caret-color"));
    pane.add(caretColorButton);

    pane.add(new JLabel("Look and Feel:"));
    UIManager.LookAndFeelInfo[] lafs = UIManager.getInstalledLookAndFeels();
    for(int i=0; i<lafs.length; i++) {
      lafComboBox.addItem(lafs[i].getName());
      //if this is current look and feel
      if(lafs[i].getClassName() == UIManager.getLookAndFeel().getClass().getName())
        lafComboBox.setSelectedIndex(i);
    }
    pane.add(lafComboBox);*/

    okButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        ok();
      }
    });
    cancelButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        cancel();
      }
    });
    applyButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        updateProperties();
        Hexpound.propertiesChanged();
      }
    });
    //pane.add(okButton);
    getRootPane().setDefaultButton(okButton);
    //pane.add(cancelButton);
    //pane.add(applyButton);

    pack();
    setLocationRelativeTo(Hexpound.getWindow());
  }

  public void ok() {
    okPressed = true;
    updateProperties();
    Hexpound.propertiesChanged();
    setVisible(false);
  }
  public void cancel() {
    setVisible(false);
  }

  private void updateProperties() {
    try {
      int num = Integer.parseInt(bytesPerLineTextField.getText());
      if(num >= 0)
        Hexpound.setIntegerProperty("bytes-per-line", num);
    } catch(NumberFormatException e) {}
    try {
      int num = Integer.parseInt(groupingTextField.getText());
      if(num > 0)
        Hexpound.setIntegerProperty("grouping", num);
    } catch(NumberFormatException e) {}
    try {
      int num = Integer.parseInt(fontSizeTextField.getText());
      if(num > 0)
        Hexpound.setIntegerProperty("font-size", num);
    } catch(NumberFormatException e) {}
    Hexpound.setIntegerProperty("font-style", (boldCheckBox.isSelected() ? Font.BOLD : 0) | (italicCheckBox.isSelected() ? Font.ITALIC : 0));
    if(unknownCharTextField.getText().length()>=1)
      Hexpound.setProperty("unknown-char", unknownCharTextField.getText());
    Hexpound.setBooleanProperty("show-only-table-chars", showOnlyTableCharsCheckBox.isSelected());
    Hexpound.setBooleanProperty("smooth-text", smoothTextCheckBox.isSelected());
    Hexpound.setColorProperty("offset-column-color", offsetColumnColorButton.getColor());
    Hexpound.setColorProperty("odd-column-color", oddColumnColorButton.getColor());
    Hexpound.setColorProperty("even-column-color", evenColumnColorButton.getColor());
    Hexpound.setColorProperty("text-column-color", textColumnColorButton.getColor());
    Hexpound.setColorProperty("caret-color", caretColorButton.getColor());
    Hexpound.setProperty("look-and-feel", UIManager.getInstalledLookAndFeels()[lafComboBox.getSelectedIndex()].getClassName());
    Hexpound.setBooleanProperty("decorate-frames", decorateFramesCheckBox.isSelected());
    Hexpound.setBooleanProperty("decorate-dialogs", decorateDialogsCheckBox.isSelected());
    Hexpound.setProperty("tab-placement", tabPlacementComboBox.getSelectedItem().toString());
  }

  /*
   * Returns true if ok was pressed, false otherwise.
   */
  public boolean showDialog() {
    setVisible(true);
    return okPressed;
  }
}
