/*
 * StatusBar.java - A status bar shown in the main window
 * Copyright (C) 2004, 2005 Jordan Miner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package hexpound;

import java.awt.BorderLayout;
import java.awt.datatransfer.StringSelection;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Toolkit;
import javax.swing.*;

public class StatusBar extends JPanel {
  private static JPanel leftPane = new JPanel(new FlowLayout(FlowLayout.LEFT, 5, 0));
  private static JLabel offset = new JLabel(" ");
  private static JLabel table = new JLabel();
  private static JLabel ovr = new JLabel();

  StatusBar() {
    super(new BorderLayout(5, 0));
    setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
    offset.addMouseListener(new MouseAdapter() {
      public void mouseClicked(MouseEvent e) {
        if(Hexpound.getBufferCount()<1)
          return;
        String str = "0x"+Integer.toHexString(HexpoundWindow.getCurrentHexArea().getCaretPosition()).toUpperCase();
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(new StringSelection(str), null);
      }
    });
    offset.setToolTipText("Click to copy caret position");
    leftPane.add(offset);
    leftPane.add(table);
    add(leftPane, BorderLayout.WEST);
    add(ovr, BorderLayout.EAST);
  }
  public static void update() {
    JHexArea hexArea = HexpoundWindow.getCurrentHexArea();
    if(hexArea == null) {
      offset.setText(" ");
      table.setText("");
      ovr.setText("");
    } else {
      String offsetStr = "Offset: ";
      offsetStr += "0x"+Integer.toHexString(hexArea.getSelectionStart()).toUpperCase();
      if(hexArea.hasSelection()) {
        offsetStr += " - ";
        offsetStr += "0x"+Integer.toHexString(hexArea.getSelectionEnd()).toUpperCase();
        int selLen = hexArea.getSelectionEnd()-hexArea.getSelectionStart();
        offsetStr += " (" + Integer.toString(selLen) + " byte" + (selLen>1?"s":"") + ")";
      }
      offset.setText(offsetStr);
      //offset.setText( "Offset: 0x"+Integer.toHexString(hexArea.getCaretPosition()) ).toUpperCase();
      if(Hexpound.getCurrentBuffer().getTable() == null)
        table.setText("Table: none");
      else
        table.setText("Table: "+Hexpound.getCurrentBuffer().getTable().getFile().getName());
      if(hexArea.isInsertEnabled())
        ovr.setText("INS");
      else
        ovr.setText("OVR");
    }
  }
}
