package hexpound;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JProgressBar;

/** This was made so you can call paintImmediately() on the progress bar */
public class ProgressDialog extends JDialog {
  protected int minimum;
  protected int maximum;
  protected JProgressBar bar = new JProgressBar();
  public ProgressDialog(JFrame parent, int min, int max) {
    super(parent, "Progress");
    setMinimum(min);
    setMaximum(max);
    getContentPane().add(bar);
    pack();
    setLocationRelativeTo(parent);
  }
  public JProgressBar getProgressBar() {
    return bar;
  }
  public void setMinimum(int min) {
    minimum = min;
    bar.setMinimum(min);
  }
  public void setMaximum(int max) {
    maximum = max;
    bar.setMaximum(max);
  }
  public void setValue(int progress) {
    if(progress==maximum)
      setVisible(false);
    bar.setValue(progress);
  }
}

