/*
 * ActionBinder.java - Associates a button with an action
 * Copyright (C) 2005 Jordan Miner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package hexpound;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.AbstractButton;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.JMenuItem;

/* Binds an action to a button (JButton, JMenuItem, JToggleButton). This will update the component when the action is updated. */
public class ActionBinder implements ChangeListener, ActionListener {
  private AbstractButton button;
  private AdvancedAction action;

  //neither of the arguments may be null
  public void bind(AbstractButton button, AdvancedAction action) {
    this.button = button;
    this.action = action;
    button.addActionListener(this);
    action.addChangeListener(this);
    stateChanged(new ChangeEvent(this));
  }
  //you must call unbind for the button to be garbage collected
  public void unbind() {
    button.removeActionListener(this);
    action.removeChangeListener(this);
    button = null;
    action = null;
  }
  public AbstractButton getButton() {
    return button;
  }
  public AdvancedAction getAction() {
    return action;
  }
  public void stateChanged(ChangeEvent e) {
    if(button instanceof JMenuItem || action.getIcon() == null)
      button.setText(action.getLabel());
    // New Swing versions use your icon as check mark, so don't set an icon for them.
    if(!action.isToggle())
      button.setIcon(action.getIcon());
    button.setRolloverIcon(action.getRolloverIcon());
    button.setEnabled(action.isEnabled());
    button.setSelected(action.isSelected());
    if(button instanceof JMenuItem)
      ((JMenuItem)button).setAccelerator(action.getAccelerator());
    else
      button.setToolTipText(action.getToolTipText());
  }
  public void actionPerformed(ActionEvent e) {
    action.invoke();
  }
}
